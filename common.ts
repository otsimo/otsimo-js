export interface Child {
  firstname: string;
  lastname: string;
  id: string;
  gameid: string;
  language: string;
  locale?: string;
}

export interface GameManifest {
  unique_name: string;
  kv_path: string;
  settings: string;
  languages: string[];
  default_language: string;
}

export interface InitOptions {
  firstname?: string;
  lastname?: string;
  childid?: string;
  gameid?: string;
  language?: string;
  locale?: string;
  width?: number;
  height?: number;
  capabilities?: string[];
  debug?: boolean;
  manifestPrefix?: string;
}

export interface SettingsCallback {
  (setting: any, sound: boolean): void;
}

export interface ResoulutionCallback {
  (width: number, height: number, orientation: string): void;
}

export type InputSource = "camera" | "mic" | "image" | "audio";

export interface MLApi {
  loadGraph(filepath: string);
  predict(input: any, cb: (result: any) => void);
  predictFromStream(source: InputSource, cb: (result: any) => void);
  closeStream();
}

export interface SettingsSaver {
  store(data: any);
  load(callback: (err?: any, data?: any) => void);
}

export interface FileApi {
  saveImageUrlData(data: string, cb: (error?: any) => void);
  saveLocal(
    id: string,
    ext: string,
    data: string,
    annotations: { [key: string]: string },
    cb: (error?: any) => void,
  );
  storeSmall(
    metadata: OtsimoFileMetadata,
    isPublic: boolean,
    data: string,
    cb: (error?: any, file?: OtsimoFileMetadata) => void,
  );

  storeBig(
    metadata: OtsimoFileMetadata,
    isPublic: boolean,
    cb: (error?: any, uploadUrl?: string) => void,
  );
  lookup(
    selector: LabelSelector,
    cb: (error?: any, files?: OtsimoFileMetadata[]) => void,
  );
}

export interface Otsimo<T, C, S> {
  settings: Object;
  kv: any;
  child: Child;
  manifest: GameManifest;
  readonly width: number;
  readonly height: number;
  readonly isWKWebView: boolean;
  readonly iOS: boolean;
  readonly android: boolean;
  readonly androidVersion: number;
  readonly capabilities: string[];
  readonly isGamePauseSupported: boolean;

  readonly tts: T;
  readonly camera: C;
  readonly speech: S;
  readonly ml: MLApi;
  readonly save: SettingsSaver;
  readonly file: FileApi;

  log(message?: any, ...optionalParams: any[]): void;
  customevent(eventName: string, data?: Object): void;
  pause(): boolean;
  quitgame(): void;
  run(cb: () => void): void;
  onSettingsChanged(cb: SettingsCallback): void;
  onResolutionChanged(cb: ResoulutionCallback): void;
  onResume(cb: () => void): void;
  init(options?: InitOptions): void;
}

export class OtsimoFileMetadata {
  key: string;
  collection: string;
  labels: { [key: string]: string };
  type: string;
  checksum: string;
  url?: string;
}

export function getAllUrlParams() {
  // get query string from url (optional) or window
  let queryString = window.location.search.slice(1);

  // we'll store the parameters here
  let obj = {};
  // if query string exists
  if (queryString) {
    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split("#")[0];

    // split our query string into its component parts
    var arr = queryString.split("&");

    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split("=");

      // in case params look like: list[]=thing1&list[]=thing2
      var paramNum = undefined;
      var paramName = a[0].replace(/\[\d*\]/, function (v) {
        paramNum = v.slice(1, -1);
        return "";
      });

      // set parameter value (use 'true' if empty)
      var paramValue = typeof a[1] === "undefined" ? "true" : a[1];

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      paramValue = paramValue.toLowerCase();

      // if parameter name already exists
      if (obj[paramName]) {
        // convert value to array (if still string)
        if (typeof obj[paramName] === "string") {
          obj[paramName] = [obj[paramName]];
        }
        // if no array index number specified...
        if (typeof paramNum === "undefined") {
          // put the value on the end of the array
          obj[paramName].push(paramValue);
        } else {
          // if array index number specified...
          // put the value at that index number
          obj[paramName][paramNum] = paramValue;
        }
      } else {
        // if param name doesn't exist yet, set it
        obj[paramName] = paramValue;
      }
    }
  }
  return obj;
}

export function makeid(length?: number) {
  if (!length) {
    length = 5;
  }
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}

/**
A Label selector operator is the set of operators that can be used in
a Label selector requirement.
*/
export type SelectorOperator =
  | "In"
  | "NotIn"
  | "Exists"
  | "DoesNotExist"
  | "Gt"
  | "Lt";
export const Operator_In: SelectorOperator = "In";
export const Operator_NotIn: SelectorOperator = "NotIn";
export const Operator_Exists: SelectorOperator = "Exists";
export const Operator_DoesNotExist: SelectorOperator = "DoesNotExist";
export const Operator_Gt: SelectorOperator = "Gt";
export const Operator_Lt: SelectorOperator = "Lt";

/**
A Label selector requirement is a selector that contains values, a key, and an operator
that relates the key and values.
*/
export class SelectorRequirement {
  /**
    key is the label key that the selector applies to.
    */
  key: string;
  /**
    operator represents a key's relationship to a set of values.
    Valid operators are In, NotIn, Exists, DoesNotExist. Gt, and Lt.
    */
  operator: SelectorOperator;
  /**
    values is an array of string values. If the operator is In or NotIn,
    the values array must be non-empty. If the operator is Exists or DoesNotExist,
    the values array must be empty. If the operator is Gt or Lt, the values
    array must have a single element, which will be interpreted as an integer.
    */
  values: string[];
}

/**
An empty Label selector term matches all objects. A null Label selector term
matches no objects.
*/
export class SelectorTerm {
  /**
    expressions is a list of Label selector requirements. The requirements are ANDed.
    */
  expressions: SelectorRequirement[];
}

/**
A Label selector represents the union of the results of one or more label queries
over a set of labels; that is, it represents the OR of the selectors represented
by the labelSelectorTerms.
*/
export class LabelSelector {
  /**
    terms is a list of label selector terms. The terms are ORed.
    */
  terms: SelectorTerm[];
}

export interface StoredCallback {
  id: string;
  collection: string;
  fn: any;
}

export class CallbackStore {
  private cbs: StoredCallback[] = [];

  store(collection: string, cb: any): string {
    const id = makeid(10);
    const obj = {
      id: id,
      collection: collection,
      fn: cb,
    } as StoredCallback;
    this.cbs.push(obj);
    return id;
  }

  getByCollection(collection: string): StoredCallback[] {
    const ls = [];
    for (const o of this.cbs) {
      if (o.collection == collection) {
        ls.push(o);
      }
    }
    return ls;
  }

  remove(id: string) {
    const index = this.cbs.findIndex((c) => c.id === id);
    if (index >= 0) {
      this.cbs.splice(index, 1);
    }
  }

  find(id: string): StoredCallback | undefined {
    for (const o of this.cbs) {
      if (o.id == id) {
        return o;
      }
    }
  }
}

export const callbacks = new CallbackStore();
