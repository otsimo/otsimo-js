import {
  OtsimoFileMetadata,
  LabelSelector,
  Otsimo,
  callbacks,
  FileApi,
} from "./common";
import oandroid from "./android";
import * as pb from "./android_pb";
import * as base64 from "@protobufjs/base64";

export class FileStore implements FileApi {
  constructor(private otsimo: Otsimo<any, any, any>) {}

  saveImageUrlData(data: string, cb: (error?: any) => void) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let id = callbacks.store("filestore", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.error);
      });
      w.webkit.messageHandlers.file.postMessage(
        JSON.stringify({
          rpc: "image",
          callback: id,
          data: data,
        }),
      );
    } else if (this.otsimo.android) {
      if (this.otsimo.androidVersion >= 20) {
        let id = callbacks.store("filestore", (id: string, result: any) => {
          callbacks.remove(id);
          cb(result.error);
        });
        oandroid.saveImageUrlData(data, id);
      }
    } else {
      this.otsimo.log("store small called");
    }
  }

  saveLocal(
    id: string,
    ext: string,
    data: string,
    annotations: { [key: string]: string },
    cb: (error?: any) => void,
  ) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let cid = callbacks.store("filestore", (cid: string, result: any) => {
        callbacks.remove(cid);
        cb(result.error);
      });
      w.webkit.messageHandlers.file.postMessage(
        JSON.stringify({
          rpc: "local",
          data: data,
          metadata: {
            key: id,
            url: id + ext,
            labels: annotations,
          } as OtsimoFileMetadata,
          callback: cid,
        }),
      );
    } else if (this.otsimo.android) {
      if (this.otsimo.androidVersion >= 20) {
        let cid = callbacks.store("filestore", (id: string, result: any) => {
          callbacks.remove(id);
          cb(result.error);
        });
        oandroid.localFile(id, id + ext, data, cid);
      }
    } else {
      this.otsimo.log("store small called");
    }
  }

  storeSmall(
    metadata: OtsimoFileMetadata,
    isPublic: boolean,
    data: string,
    cb: (error?: any, file?: OtsimoFileMetadata) => void,
  ) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let id = callbacks.store("filestore", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.error, result.metadata);
      });
      w.webkit.messageHandlers.file.postMessage(
        JSON.stringify({
          rpc: "small",
          metadata: metadata,
          callback: id,
          data: data,
          public: isPublic,
        }),
      );
    } else if (this.otsimo.android) {
      if (this.otsimo.androidVersion >= 20) {
        let id = callbacks.store("filestore", (id: string, result: any) => {
          callbacks.remove(id);
          cb(result.error, result.metadata);
        });
        oandroid.storeSmall(JSON.stringify(metadata), data, isPublic, id);
      }
    } else {
      this.otsimo.log("store small called");
    }
  }

  storeBig(
    metadata: OtsimoFileMetadata,
    isPublic: boolean,
    cb: (error?: any, uploadUrl?: string) => void,
  ) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let id = callbacks.store("filestore", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.error, result.url);
      });
      w.webkit.messageHandlers.file.postMessage(
        JSON.stringify({
          rpc: "big",
          callback: id,
          public: isPublic,
          metadata: metadata,
        }),
      );
    } else if (this.otsimo.android) {
      if (this.otsimo.androidVersion >= 20) {
        let id = callbacks.store("filestore", (id: string, result: any) => {
          callbacks.remove(id);
          cb(result.error, result.url);
        });
        oandroid.storeBig(JSON.stringify(metadata), isPublic, id);
      }
    }
  }

  private convertSelectors(s: LabelSelector): pb.apipb.ILabelSelector {
    const ls: pb.apipb.ILabelSelector = { terms: [] };
    for (let t of s.terms || []) {
      ls.terms.push({
        expressions: t.expressions.map(e => ({
          key: e.key,
          values: e.values,
          operator: pb.apipb.LabelSelectorOperator[e.operator],
        })),
      });
    }
    return ls;
  }

  lookup(
    selector: LabelSelector,
    cb: (error?: any, files?: OtsimoFileMetadata[]) => void,
  ) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let id = callbacks.store("filestore", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.error, result.files);
      });
      w.webkit.messageHandlers.file.postMessage(
        JSON.stringify({
          rpc: "lookup",
          callback: id,
          selector: selector,
        }),
      );
    } else if (this.otsimo.android) {
      if (this.otsimo.androidVersion >= 20) {
        console.log("lookup : andr ver >= 20");
        const lsr = this.convertSelectors(selector);
        const bytes: Uint8Array = pb.apipb.LabelSelector.encode(lsr).finish();
        const data = base64.encode(bytes, 0, bytes.length);
        let id = callbacks.store("filestore", (id: string, result: any) => {
          callbacks.remove(id);
          cb(result.error, result.files);
        });
        oandroid.lookup(data, id);
      }
    }
  }
}
