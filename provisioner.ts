export interface Provisioner {
  endGameEntertainmentStarted();
  endGameEntertainmentCompleted();
  sessionStarted();
  sessionCompleted(starEarned: number);
  stepCompleted(step: number);
  shouldCreateNewSession(): boolean;
  canStartGame(gameId?: string): boolean;
  pauseGame(): boolean;
}

const Provisioner: Provisioner = (window.parent as any).__OtsimoProvisioner;
export default Provisioner;
