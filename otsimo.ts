import { TTS } from "./tts";
import { Speech } from "./speech";
import { Camera } from "./camera";
import { ML } from "./ml";
import { FileStore } from "./file";
import { PersistentSettings } from "./save";
import android from "./android";
import ios from "./ios";
import Provisioner from "./provisioner";

import {
  Child,
  GameManifest,
  Otsimo,
  InitOptions,
  SettingsCallback,
  ResoulutionCallback,
  getAllUrlParams,
  callbacks,
  SettingsSaver,
  MLApi,
  FileApi,
} from "./common";

interface NativeInitOptions {
  child: Child;
  sound: boolean;
  settings: Object;
  root: string;
  capabilities?: string[];
  screen: { width: number; height: number };
}

function navigatorLanguages(nav: any): string[] {
  let found = [];
  if (typeof nav.languages !== "undefined") {
    // chrome only; not an array, so can't use .push.apply instead of iterating
    for (var i = 0; i < nav.languages.length; i++) {
      found.push(nav.languages[i]);
    }
  }
  if (typeof nav.userLanguage !== "undefined") {
    found.push(nav.userLanguage);
  }
  if (nav.language) {
    found.push(nav.language);
  }
  return found;
}

const SETTINGS_CB_KEY = "settings";
const RUN_CB_KEY = "run";
const RESOLUTION_CB_KEY = "resoulution";
const RESTART_CB_KEY = "restart";
const RESUME_CB_KEY = "resume";

export class OtsimoHelper implements Otsimo<TTS, Camera, Speech> {
  settings: Object = {};
  kv: any = null;
  child: Child = null;
  manifest: GameManifest = null;

  private _canStartGame: boolean = false;
  private _tts = new TTS(this);
  private _ml = new ML(this);
  private _camera = new Camera(this);
  private _speech = new Speech(this);
  private _file = new FileStore(this);
  private _save = new PersistentSettings(this);

  private _debug = false;
  private _sound = true;
  private _root = "";
  private _width: number = 1024;
  private _height: number = 768;
  private _isLoaded: boolean = false;
  private _capabilities: string[] = ["sandbox"];
  private _manifestPrefix = "";

  constructor() {
    if (this.android) {
      this.registerAndroidMessages();
    }
  }

  get ml(): MLApi {
    return this._ml;
  }

  get tts(): TTS {
    return this._tts;
  }

  get camera(): Camera {
    return this._camera;
  }

  get speech(): Speech {
    return this._speech;
  }

  get file(): FileApi {
    return this._file;
  }

  get sound(): boolean {
    return this._sound;
  }

  get save(): SettingsSaver {
    return this._save;
  }

  get width(): number {
    return this._width;
  }

  get height(): number {
    return this._height;
  }

  get isWKWebView(): boolean {
    if (typeof window === "undefined") {
      return false;
    }
    let w = window as any;
    return !!(w && w.webkit && w.webkit.messageHandlers);
  }

  get iOS(): boolean {
    if (typeof navigator === "undefined") {
      return false;
    }
    return navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
  }

  get android(): boolean {
    if (typeof navigator === "undefined") {
      return false;
    }
    return /OtsimoChildApp\/[0-9\.]+$/.test(navigator.userAgent);
  }

  get inProvisioner(): boolean {
    return !!Provisioner;
  }
  get isGamePauseSupported(): boolean {
    return !!Provisioner && typeof Provisioner.pauseGame === "function";
  }
  get androidVersion(): number {
    if (this.android) {
      const fullVersion = navigator.userAgent.substr("OtsimoChildApp/".length);
      const dotLoc = fullVersion.indexOf("."); //2.0
      return (
        parseFloat(fullVersion) * Math.pow(10, fullVersion.length - dotLoc - 1)
      );
    } else {
      return -1;
    }
  }

  get capabilities(): string[] {
    return this._capabilities;
  }

  log(message?: any, ...optionalParams: any[]) {
    console.log.apply(console, arguments);
    if (this.isWKWebView) {
      ios().console.postMessage(JSON.stringify(arguments));
    } else if (this.android) {
      (window as any).postMessage(
        JSON.stringify({ action: "log", arguments: arguments }),
        "*",
      );
    }
  }

  customevent(
    eventName: string,
    data?: { [key: string]: number | boolean | string },
  ) {
    let _nd = {};
    if (typeof data === "object") {
      Object.keys(data).forEach((k) => {
        if (typeof data[k] !== "undefined") {
          _nd[k] = data[k];
        } else {
          console.error(
            "event key",
            k,
            "invalid. it has",
            typeof data[k],
            "type",
          );
        }
      });
    }
    if (this.isWKWebView) {
      ios().analytics.postMessage({
        event: eventName,
        data: _nd,
      });
    } else if (this.android) {
      if (this.androidVersion >= 20) {
        android.customEvent(eventName, JSON.stringify(_nd));
      } else {
        window.postMessage(
          JSON.stringify({
            action: "customevent",
            event: eventName,
            data: _nd,
          }),
          "*",
        );
      }
    } else {
      this.log("customevent", eventName, _nd);
    }
  }

  pause(): boolean {
    if (Provisioner && typeof Provisioner.pauseGame === "function") {
      return Provisioner.pauseGame();
    }
    return false;
  }
  onResume(cb: () => void) {
    callbacks.store(RESUME_CB_KEY, cb);
  }
  quitgame() {
    if (this.isWKWebView) {
      ios().player.postMessage({
        event: "quitgame",
      });
    } else if (this.android) {
      if (this.androidVersion >= 20) {
        android.quitGame();
      } else {
        window.postMessage(JSON.stringify({ action: "quitgame" }), "*");
      }
    } else {
      this.log("quit game called");
    }
  }

  run(cb: () => void) {
    this.log("register function to run");
    if (this._isLoaded && this.canStartGame) {
      cb();
    } else {
      callbacks.store(RUN_CB_KEY, cb);
    }
  }

  onRestartRequested(cb: () => void) {
    callbacks.store(RESTART_CB_KEY, cb);
  }

  get canStartGame() {
    if (this.inProvisioner) {
      return this._canStartGame || Provisioner.canStartGame(window.name);
    } else {
      return true;
    }
  }

  setCanStartGame(canStart: boolean) {
    if (canStart === true) {
      if (this._isLoaded) {
        const cbs = callbacks.getByCollection(RUN_CB_KEY);
        for (const cb of cbs) {
          cb.fn();
          callbacks.remove(cb.id);
        }
      }
    }
    this._canStartGame = canStart;
  }

  onSettingsChanged(cb: SettingsCallback) {
    callbacks.store(SETTINGS_CB_KEY, cb);
  }

  onResolutionChanged(cb: ResoulutionCallback) {
    callbacks.store(RESOLUTION_CB_KEY, cb);
  }

  endGameEntertainmentStarted() {
    if (Provisioner) {
      Provisioner.endGameEntertainmentStarted();
    }
  }

  endGameEntertainmentCompleted() {
    if (Provisioner) {
      Provisioner.endGameEntertainmentCompleted();
    }
  }
  sessionStarted() {
    if (Provisioner && typeof Provisioner.sessionStarted === "function") {
      Provisioner.sessionStarted();
    }
  }
  stepCompleted(step: number) {
    if (Provisioner && typeof Provisioner.stepCompleted === "function") {
      Provisioner.stepCompleted(step);
    }
  }
  sessionCompleted(starCount: number) {
    if (Provisioner && typeof Provisioner.sessionCompleted === "function") {
      Provisioner.sessionCompleted(starCount);
    }
  }

  shouldCreateNewSession() {
    if (Provisioner) {
      return Provisioner.shouldCreateNewSession();
    }
    return true;
  }

  private initByUrl(params: { [key: string]: string }) {
    let obj: NativeInitOptions = {
      child: {
        firstname: params["fn"],
        lastname: params["ln"],
        id: params["id"],
        language: params["lang"],
        locale: params["locale"],
        gameid: "",
      },
      capabilities: (params["c"] as any) as string[],
      sound: params["sound"] === "true" || params["sound"] === "1",
      root: params["root"] || "",
      screen: {
        width: Number(params["w"]),
        height: Number(params["h"]),
      },
      settings: {},
    };
    if (window.location.hash.length > 1) {
      try {
        obj.settings = JSON.parse(window.location.hash.substr(1));
      } catch (err) {
        console.log("failed to parse settings");
      }
    }
    this.__init(obj);
  }

  init(options?: InitOptions) {
    if (!options) {
      options = {};
    }
    this.log("initialize the bundle otsimo.js");
    if (this.isWKWebView || (this.android && this.androidVersion >= 20)) {
      this.log("sandbox won't be initializing");
      this.__isready();
      return;
    }
    const params = getAllUrlParams();
    if (params["byurl"] === "1") {
      return this.initByUrl(params);
    }
    if (params["lang"]) {
      options.language = params["lang"];
    }
    if (params["locale"]) {
      options.locale = params["locale"];
    }
    if (params["c"]) {
      options.capabilities = params["c"];
    }
    this.child = {
      firstname: options.firstname || "debug",
      lastname: options.lastname || "user",
      id: options.childid || "",
      gameid: options.gameid || "",
      language: options.language || this.getLanguages()[0],
      locale: options.locale,
    };
    this._width = options.width || 1024;
    this._height = options.height || 768;
    if (options.capabilities && Array.isArray(options.capabilities)) {
      this._capabilities = options.capabilities;
    }
    this._debug = !(options.debug === false);
    if (options.manifestPrefix) {
      this._manifestPrefix = options.manifestPrefix;
    }
    this.getJSON(
      this._manifestPrefix + "otsimo.json",
      this.__initManifest.bind(this),
    );
  }

  private getLanguages(): string[] {
    let found: string[] = [];
    if (typeof navigator !== "undefined") {
      found = navigatorLanguages(navigator);
    }
    for (var iif = 0; iif < found.length; iif++) {
      if (found[iif].indexOf("-") > -1) {
        found[iif] = found[iif].split("-")[0];
      }
    }
    return found;
  }

  private registerAndroidMessages() {
    document.addEventListener("message", (e: any) => {
      try {
        var messageData = JSON.parse(e.data);
        var fn = this[messageData.func];
        if (typeof fn === "function") {
          fn.apply(this, messageData.args);
        }
      } catch (err) {
        console.warn(err);
      }
    });
  }
  private __callSettingsCallbacks(settings: any, sound: boolean) {
    if (settings) {
      this.settings = settings;
    }
    this._sound = sound;
    let scb = callbacks.getByCollection(SETTINGS_CB_KEY);
    for (let cb of scb) {
      cb.fn(settings, sound);
    }
  }

  private __callResolutionCallbacks(
    width: number,
    height: number,
    orientation: string,
  ) {
    const cbs = callbacks.getByCollection(RESOLUTION_CB_KEY);
    for (let cb of cbs) {
      cb.fn(width, height, orientation);
    }
    this._width = width;
    this._height = height;
  }

  private __callLoadingCallbacks() {
    this._isLoaded = true;
    if (!this.canStartGame) {
      return;
    }
    const cbs = callbacks.getByCollection(RUN_CB_KEY);
    for (const cb of cbs) {
      cb.fn();
      callbacks.remove(cb.id);
    }
  }
  private __callResumeCallbacks() {
    let scb = callbacks.getByCollection(RESUME_CB_KEY);
    for (let cb of scb) {
      cb.fn();
    }
  }

  private __loadKeyValueStore() {
    const sy = this.getLanguages();
    let childLang = this.child.language;
    if (this.manifest.languages.indexOf(childLang) < 0) {
      childLang = this.manifest.default_language;
    }
    this.getJSON(
      this._manifestPrefix + this.manifest.kv_path + "/" + childLang + ".json",
      (err, data) => {
        if (err) {
          this.log("failed to get kv, status", err);
        } else {
          this.kv = data;
          this.log("otsimo initialized");
          this.__callLoadingCallbacks();
        }
      },
    );
  }

  private __initSettings(err?: any, data?: any) {
    if (err) {
      this.log("failed to get settings,status", err);
    } else {
      this.log("settings", data);
      var ks = Object.keys(data.properties);
      for (var i = 0; i < ks.length; ++i) {
        var p = data.properties[ks[i]];
        this.settings[p.id] = p.default;
      }
      this.__loadKeyValueStore();
    }
  }

  private __initManifest(err?: any, data?: any) {
    if (err) {
      this.log("Failed to get otsimo.json, status=", err);
    } else {
      this.manifest = data;
      if (typeof data.options !== "undefined") {
        const params = getAllUrlParams();
        Object.keys(data.options).forEach((k) => {
          const opt = data.options[k];
          let val = opt.default;
          if (typeof params[opt.id] !== "undefined") {
            val = params[opt.id];
          }
          if (opt.type === "boolean") {
            this.settings[opt.id] = val === "true";
          } else if (opt.type === "integer") {
            this.settings[opt.id] = Number(val);
          } else {
            this.settings[opt.id] = val;
          }
        });
        this.__loadKeyValueStore();
      } else {
        this.getJSON(
          this._manifestPrefix + this.manifest.settings,
          this.__initSettings.bind(this),
        );
      }
    }
  }

  private __init(options: NativeInitOptions) {
    this.log("__init called", options);
    this.settings = options.settings;
    this.child = options.child;
    this._width = options.screen.width;
    this._height = options.screen.height;
    this._root = options.root || "";
    this._sound = options.sound;
    this._capabilities = options.capabilities || ["sandbox"];
    this.getJSON(this._root + "otsimo.json", (err, manifest) => {
      if (err) {
        this.log("Failed to get otsimo.json, status=", err);
        return this.quitgame();
      }
      this.manifest = manifest;
      let childLang = this.child.language;
      if (this.manifest.languages.indexOf(childLang) < 0) {
        childLang = this.manifest.default_language;
      }
      let langFile =
        this._root + this.manifest.kv_path + "/" + childLang + ".json";
      this.getJSON(langFile, (err, kv) => {
        if (err) {
          this.log("failed to get kv, status", err);
          return this.quitgame();
        }
        this.kv = kv;
        this.log("initialized");
        this.__callLoadingCallbacks();
      });
    });
  }

  private getJSON(url, res: (err?: any, data?: any) => void) {
    if (typeof XMLHttpRequest === "undefined") {
      return res("XMLHttpRequest does not exist");
    }
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = () => {
      if (xmlhttp.readyState == 4) {
        let status = xmlhttp.status;
        if (status === 200 || status === 0) {
          try {
            let data = JSON.parse(xmlhttp.responseText);
            res(null, data);
          } catch (err) {
            res(err);
          }
        } else {
          res(status);
        }
      }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
  }

  __isready() {
    if (!this.inProvisioner) {
      return;
    }
    if (this.isWKWebView) {
      ios().player.postMessage({
        event: "isready",
        gameid: window.name, // window.name is gameid at provisioner
      });
    } else if (this.android) {
      if (android.isReady) {
        android.isReady(window.name); // window.name is gameid at provisioner
      }
    }
  }

  private __callback(id: string, data: any) {
    let cb = callbacks.find(id);
    if (typeof cb !== "undefined") {
      cb.fn(id, data);
    }
  }

  private __callbackCollection(coll: string, data?: any) {
    const cbs = callbacks.getByCollection(coll);
    for (const cb of cbs) {
      cb.fn(data);
    }
  }

  public restart() {
    this.__callbackCollection(RESTART_CB_KEY);
  }
}

const otsimo = new OtsimoHelper();
export default otsimo;
export { OtsimoFileMetadata, callbacks } from "./common";
if (typeof window !== "undefined") {
  (window as any).otsimo = otsimo;
}
