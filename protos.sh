FILES="./android.proto"

pbjs -p ./protos \
    -t static-module \
    -w commonjs \
    -o ./android_pb.js \
    ${FILES}

pbts -o ./android_pb.d.ts ./android_pb.js
