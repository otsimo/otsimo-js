export interface OtsimoAndroid {
  quitGame();
  customEvent(eventName: string, data: string);
  saveImageUrlData(data: string, cb: string);
  storeSmall(metadata: string, data: string, isPublic: boolean, id: string);
  localFile(id: string, name: string, data: string, cid: string);
  storeSmall(metadata: string, data: string, isPublic: boolean, id: string);
  storeBig(metadata: string, isPublic: boolean, id: string);
  lookup(selector: string, id: string);
  ttsSpeak(text: string);
  isReady(gameId: string);
  authorizationStatus(id: string);
  requestAuthorization(id: string);
  speechRecognize(partialResults: boolean, id: string);
  speechCancel();
  settingsStore(data: string);
  settingsLoad(id: string);
}

const oandroid: OtsimoAndroid = (window as any).OtsimoAndroidGameInterface;
export default oandroid;
