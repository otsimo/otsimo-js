import { Otsimo, callbacks, SettingsSaver } from "./common";
import android from "./android";
export class PersistentSettings implements SettingsSaver {
  constructor(private otsimo: Otsimo<any, any, any>) {}

  store(data: any) {
    if (typeof data === "undefined") {
      console.error("save data cannot be undefined");
      return;
    }
    var sdata = JSON.stringify(data);
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      w.webkit.messageHandlers.save.postMessage({
        event: "save",
        data: sdata,
      });
    } else if (this.otsimo.android) {
      if (android && android.settingsStore) {
        android.settingsStore(sdata);
      } else {
        window.postMessage(
          JSON.stringify({ action: "save", event: "save", data: data }),
          "*",
        );
      }
    } else {
      this.otsimo.log("saveLocalSettings", data);
      localStorage.setItem(this.otsimo.manifest.unique_name, sdata);
    }
  }

  load(callback: (err?: any, data?: any) => void) {
    let id = callbacks.store("localsettings", (id: string, data: any) => {
      callbacks.remove(id);
      if (typeof data === "string") {
        try {
          var odata = JSON.parse(data);
          callback(null, odata);
        } catch (error) {
          callback(error);
        }
      } else {
        callback("local settings result is invalid");
      }
    });
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      w.webkit.messageHandlers.save.postMessage({
        event: "load",
        id: id,
      });
    } else if (this.otsimo.android) {
      if (android && android.settingsLoad) {
        android.settingsLoad(id);
      } else {
        callback(new Error("not supported"), null);
      }
    } else {
      try {
        var sdata = localStorage.getItem(this.otsimo.manifest.unique_name);
        var data = JSON.parse(sdata);
        callback(null, data);
      } catch (err) {
        callback(err, null);
      }
    }
  }
}
