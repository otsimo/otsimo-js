import { Otsimo, callbacks } from "./common";
import oandroid from "./android";
export type SpeechAuthStatus =
  | "notDetermined"
  | "denied"
  | "restricted"
  | "authorized";

export interface SpeechConfiguration {
  partialResults?: boolean;
  highPrecision?: boolean;
}

export interface TranscriptionSegment {
  substring: string;
  duration: number;
  confidence: number;
  alternatives: string[];
}

export interface Transcription {
  formatted: string;
  segments: TranscriptionSegment[];
}

export interface SpeechRecognitionResult {
  final: boolean;
  transcriptions: Transcription[];
  error: string;
  available: boolean;
}

export interface SpeechRecognitionResultCallback {
  (result: SpeechRecognitionResult): void;
}

export class Speech {
  constructor(private otsimo: Otsimo<any, any, any>) {}

  authorizationStatus(cb: (status: SpeechAuthStatus) => void) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let id = callbacks.store("speech", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.status);
      });
      w.webkit.messageHandlers.speech.postMessage(
        JSON.stringify({
          rpc: "status",
          callback: id,
        }),
      );
    } else if (this.otsimo.android) {
      let id = callbacks.store("speech", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.status);
      });
      oandroid.authorizationStatus(id);
    }
  }

  requestAuthorization(cb: (status: SpeechAuthStatus) => void) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let id = callbacks.store("speech", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.status);
      });
      w.webkit.messageHandlers.speech.postMessage(
        JSON.stringify({
          rpc: "request",
          callback: id,
        }),
      );
    } else if (this.otsimo.android) {
      let id = callbacks.store("speech", (id: string, result: any) => {
        callbacks.remove(id);
        cb(result.status);
      });
      oandroid.requestAuthorization(id);
    }
  }

  cancel() {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      w.webkit.messageHandlers.speech.postMessage(
        JSON.stringify({
          rpc: "cancel",
        }),
      );
    } else if (this.otsimo.android) {
      oandroid.speechCancel();
    }
  }

  recognize(options: SpeechConfiguration, cb: SpeechRecognitionResultCallback) {
    if (this.otsimo.isWKWebView) {
      let w: any = window;
      let id = callbacks.store(
        "speech",
        (id: string, result: SpeechRecognitionResult) => {
          cb(result);
          if (result.final) {
            callbacks.remove(id);
          }
        },
      );
      w.webkit.messageHandlers.speech.postMessage(
        JSON.stringify({
          rpc: "recognize",
          callback: id,
          config: options,
        }),
      );
    } else if (this.otsimo.android) {
      let id = callbacks.store(
        "speech",
        (id: string, result: SpeechRecognitionResult) => {
          cb(result);
          if (result.final) {
            callbacks.remove(id);
          }
        },
      );
      oandroid.speechRecognize(options.partialResults, id);
    }
  }
}
