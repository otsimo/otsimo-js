import Provisioner from "./provisioner";

export interface IOSMessageHandler {
  postMessage(message: any): void;
}

export interface IOSWebkitMessages {
  analytics: IOSMessageHandler;
  console: IOSMessageHandler;
  player: IOSMessageHandler;
  tts: IOSMessageHandler;
  save: IOSMessageHandler;
  provisioner: IOSMessageHandler;
  file: IOSMessageHandler;
}

export default function ios(): IOSWebkitMessages {
  /*  if (Provisioner) {
    return (window.parent as any).webkit.messageHandlers;
  }*/
  let w: any = window;
  return w.webkit.messageHandlers;
}
