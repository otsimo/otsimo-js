import * as $protobuf from "protobufjs";

/** Namespace otsimo. */
export namespace otsimo {

    /** Properties of a FileStoreRPC. */
    interface IFileStoreRPC {

        /** FileStoreRPC rpc */
        rpc?: string;

        /** FileStoreRPC metadata */
        metadata?: otsimo.IFileMetadata;

        /** FileStoreRPC callback */
        callback?: string;

        /** FileStoreRPC data */
        data?: string;

        /** FileStoreRPC public */
        "public"?: boolean;

        /** FileStoreRPC selector */
        selector?: apipb.ILabelSelector;
    }

    /** Represents a FileStoreRPC. */
    class FileStoreRPC {

        /**
         * Constructs a new FileStoreRPC.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IFileStoreRPC);

        /** FileStoreRPC rpc. */
        public rpc: string;

        /** FileStoreRPC metadata. */
        public metadata?: (otsimo.IFileMetadata|null);

        /** FileStoreRPC callback. */
        public callback: string;

        /** FileStoreRPC data. */
        public data: string;

        /** FileStoreRPC public. */
        public public_: boolean;

        /** FileStoreRPC selector. */
        public selector?: (apipb.ILabelSelector|null);

        /**
         * Creates a new FileStoreRPC instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FileStoreRPC instance
         */
        public static create(properties?: otsimo.IFileStoreRPC): otsimo.FileStoreRPC;

        /**
         * Encodes the specified FileStoreRPC message. Does not implicitly {@link otsimo.FileStoreRPC.verify|verify} messages.
         * @param message FileStoreRPC message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IFileStoreRPC, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FileStoreRPC message, length delimited. Does not implicitly {@link otsimo.FileStoreRPC.verify|verify} messages.
         * @param message FileStoreRPC message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IFileStoreRPC, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FileStoreRPC message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FileStoreRPC
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.FileStoreRPC;

        /**
         * Decodes a FileStoreRPC message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FileStoreRPC
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.FileStoreRPC;

        /**
         * Verifies a FileStoreRPC message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FileStoreRPC message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FileStoreRPC
         */
        public static fromObject(object: { [k: string]: any }): otsimo.FileStoreRPC;

        /**
         * Creates a plain object from a FileStoreRPC message. Also converts values to other types if specified.
         * @param message FileStoreRPC
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.FileStoreRPC, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FileStoreRPC to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a StoreResult. */
    interface IStoreResult {

        /** StoreResult error */
        error?: string;

        /** StoreResult metadata */
        metadata?: otsimo.IFileMetadata;
    }

    /** Represents a StoreResult. */
    class StoreResult {

        /**
         * Constructs a new StoreResult.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IStoreResult);

        /** StoreResult error. */
        public error: string;

        /** StoreResult metadata. */
        public metadata?: (otsimo.IFileMetadata|null);

        /**
         * Creates a new StoreResult instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StoreResult instance
         */
        public static create(properties?: otsimo.IStoreResult): otsimo.StoreResult;

        /**
         * Encodes the specified StoreResult message. Does not implicitly {@link otsimo.StoreResult.verify|verify} messages.
         * @param message StoreResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IStoreResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified StoreResult message, length delimited. Does not implicitly {@link otsimo.StoreResult.verify|verify} messages.
         * @param message StoreResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IStoreResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a StoreResult message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StoreResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.StoreResult;

        /**
         * Decodes a StoreResult message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StoreResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.StoreResult;

        /**
         * Verifies a StoreResult message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a StoreResult message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns StoreResult
         */
        public static fromObject(object: { [k: string]: any }): otsimo.StoreResult;

        /**
         * Creates a plain object from a StoreResult message. Also converts values to other types if specified.
         * @param message StoreResult
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.StoreResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this StoreResult to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a LookupResult. */
    interface ILookupResult {

        /** LookupResult error */
        error?: string;

        /** LookupResult files */
        files?: otsimo.IFileMetadata[];
    }

    /** Represents a LookupResult. */
    class LookupResult {

        /**
         * Constructs a new LookupResult.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.ILookupResult);

        /** LookupResult error. */
        public error: string;

        /** LookupResult files. */
        public files: otsimo.IFileMetadata[];

        /**
         * Creates a new LookupResult instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LookupResult instance
         */
        public static create(properties?: otsimo.ILookupResult): otsimo.LookupResult;

        /**
         * Encodes the specified LookupResult message. Does not implicitly {@link otsimo.LookupResult.verify|verify} messages.
         * @param message LookupResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.ILookupResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LookupResult message, length delimited. Does not implicitly {@link otsimo.LookupResult.verify|verify} messages.
         * @param message LookupResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.ILookupResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LookupResult message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LookupResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.LookupResult;

        /**
         * Decodes a LookupResult message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LookupResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.LookupResult;

        /**
         * Verifies a LookupResult message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LookupResult message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LookupResult
         */
        public static fromObject(object: { [k: string]: any }): otsimo.LookupResult;

        /**
         * Creates a plain object from a LookupResult message. Also converts values to other types if specified.
         * @param message LookupResult
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.LookupResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LookupResult to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an ImageSaveResult. */
    interface IImageSaveResult {

        /** ImageSaveResult error */
        error?: string;
    }

    /** Represents an ImageSaveResult. */
    class ImageSaveResult {

        /**
         * Constructs a new ImageSaveResult.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IImageSaveResult);

        /** ImageSaveResult error. */
        public error: string;

        /**
         * Creates a new ImageSaveResult instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ImageSaveResult instance
         */
        public static create(properties?: otsimo.IImageSaveResult): otsimo.ImageSaveResult;

        /**
         * Encodes the specified ImageSaveResult message. Does not implicitly {@link otsimo.ImageSaveResult.verify|verify} messages.
         * @param message ImageSaveResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IImageSaveResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ImageSaveResult message, length delimited. Does not implicitly {@link otsimo.ImageSaveResult.verify|verify} messages.
         * @param message ImageSaveResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IImageSaveResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an ImageSaveResult message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ImageSaveResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.ImageSaveResult;

        /**
         * Decodes an ImageSaveResult message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ImageSaveResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.ImageSaveResult;

        /**
         * Verifies an ImageSaveResult message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an ImageSaveResult message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ImageSaveResult
         */
        public static fromObject(object: { [k: string]: any }): otsimo.ImageSaveResult;

        /**
         * Creates a plain object from an ImageSaveResult message. Also converts values to other types if specified.
         * @param message ImageSaveResult
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.ImageSaveResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ImageSaveResult to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a StoreBigResult. */
    interface IStoreBigResult {

        /** StoreBigResult error */
        error?: string;

        /** StoreBigResult url */
        url?: string;
    }

    /** Represents a StoreBigResult. */
    class StoreBigResult {

        /**
         * Constructs a new StoreBigResult.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IStoreBigResult);

        /** StoreBigResult error. */
        public error: string;

        /** StoreBigResult url. */
        public url: string;

        /**
         * Creates a new StoreBigResult instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StoreBigResult instance
         */
        public static create(properties?: otsimo.IStoreBigResult): otsimo.StoreBigResult;

        /**
         * Encodes the specified StoreBigResult message. Does not implicitly {@link otsimo.StoreBigResult.verify|verify} messages.
         * @param message StoreBigResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IStoreBigResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified StoreBigResult message, length delimited. Does not implicitly {@link otsimo.StoreBigResult.verify|verify} messages.
         * @param message StoreBigResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IStoreBigResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a StoreBigResult message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StoreBigResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.StoreBigResult;

        /**
         * Decodes a StoreBigResult message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StoreBigResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.StoreBigResult;

        /**
         * Verifies a StoreBigResult message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a StoreBigResult message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns StoreBigResult
         */
        public static fromObject(object: { [k: string]: any }): otsimo.StoreBigResult;

        /**
         * Creates a plain object from a StoreBigResult message. Also converts values to other types if specified.
         * @param message StoreBigResult
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.StoreBigResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this StoreBigResult to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Represents a File */
    class File extends $protobuf.rpc.Service {

        /**
         * Constructs a new File service.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         */
        constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

        /**
         * Creates new File service using the specified rpc implementation.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         * @returns RPC service. Useful where requests and/or responses are streamed.
         */
        public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): File;

        /**
         * Calls StoreSmall.
         * @param request StoreSmallReq message or plain object
         * @param callback Node-style callback called with the error, if any, and StoreRes
         */
        public storeSmall(request: otsimo.IStoreSmallReq, callback: otsimo.File.StoreSmallCallback): void;

        /**
         * Calls StoreSmall.
         * @param request StoreSmallReq message or plain object
         * @returns Promise
         */
        public storeSmall(request: otsimo.IStoreSmallReq): Promise<otsimo.StoreRes>;

        /**
         * Calls RequestStoreBig.
         * @param request UploadReq message or plain object
         * @param callback Node-style callback called with the error, if any, and UploadRes
         */
        public requestStoreBig(request: otsimo.IUploadReq, callback: otsimo.File.RequestStoreBigCallback): void;

        /**
         * Calls RequestStoreBig.
         * @param request UploadReq message or plain object
         * @returns Promise
         */
        public requestStoreBig(request: otsimo.IUploadReq): Promise<otsimo.UploadRes>;

        /**
         * Calls Lookup.
         * @param request LookupReq message or plain object
         * @param callback Node-style callback called with the error, if any, and LookupRes
         */
        public lookup(request: otsimo.ILookupReq, callback: otsimo.File.LookupCallback): void;

        /**
         * Calls Lookup.
         * @param request LookupReq message or plain object
         * @returns Promise
         */
        public lookup(request: otsimo.ILookupReq): Promise<otsimo.LookupRes>;
    }

    namespace File {

        /**
         * Callback as used by {@link otsimo.File#storeSmall}.
         * @param error Error, if any
         * @param [response] StoreRes
         */
        type StoreSmallCallback = (error: (Error|null), response?: otsimo.StoreRes) => void;

        /**
         * Callback as used by {@link otsimo.File#requestStoreBig}.
         * @param error Error, if any
         * @param [response] UploadRes
         */
        type RequestStoreBigCallback = (error: (Error|null), response?: otsimo.UploadRes) => void;

        /**
         * Callback as used by {@link otsimo.File#lookup}.
         * @param error Error, if any
         * @param [response] LookupRes
         */
        type LookupCallback = (error: (Error|null), response?: otsimo.LookupRes) => void;
    }

    /** Properties of a FileMetadata. */
    interface IFileMetadata {

        /** FileMetadata key */
        key?: string;

        /** FileMetadata collection */
        collection?: string;

        /** FileMetadata labels */
        labels?: { [k: string]: string };

        /** FileMetadata type */
        type?: string;

        /** FileMetadata checksum */
        checksum?: string;

        /** FileMetadata owner */
        owner?: string;

        /** FileMetadata url */
        url?: string;
    }

    /** Represents a FileMetadata. */
    class FileMetadata {

        /**
         * Constructs a new FileMetadata.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IFileMetadata);

        /** FileMetadata key. */
        public key: string;

        /** FileMetadata collection. */
        public collection: string;

        /** FileMetadata labels. */
        public labels: { [k: string]: string };

        /** FileMetadata type. */
        public type: string;

        /** FileMetadata checksum. */
        public checksum: string;

        /** FileMetadata owner. */
        public owner: string;

        /** FileMetadata url. */
        public url: string;

        /**
         * Creates a new FileMetadata instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FileMetadata instance
         */
        public static create(properties?: otsimo.IFileMetadata): otsimo.FileMetadata;

        /**
         * Encodes the specified FileMetadata message. Does not implicitly {@link otsimo.FileMetadata.verify|verify} messages.
         * @param message FileMetadata message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IFileMetadata, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FileMetadata message, length delimited. Does not implicitly {@link otsimo.FileMetadata.verify|verify} messages.
         * @param message FileMetadata message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IFileMetadata, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FileMetadata message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FileMetadata
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.FileMetadata;

        /**
         * Decodes a FileMetadata message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FileMetadata
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.FileMetadata;

        /**
         * Verifies a FileMetadata message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FileMetadata message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FileMetadata
         */
        public static fromObject(object: { [k: string]: any }): otsimo.FileMetadata;

        /**
         * Creates a plain object from a FileMetadata message. Also converts values to other types if specified.
         * @param message FileMetadata
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.FileMetadata, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FileMetadata to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an UploadReq. */
    interface IUploadReq {

        /** UploadReq metadata */
        metadata?: otsimo.IFileMetadata[];
    }

    /** Represents an UploadReq. */
    class UploadReq {

        /**
         * Constructs a new UploadReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IUploadReq);

        /** UploadReq metadata. */
        public metadata: otsimo.IFileMetadata[];

        /**
         * Creates a new UploadReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UploadReq instance
         */
        public static create(properties?: otsimo.IUploadReq): otsimo.UploadReq;

        /**
         * Encodes the specified UploadReq message. Does not implicitly {@link otsimo.UploadReq.verify|verify} messages.
         * @param message UploadReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IUploadReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified UploadReq message, length delimited. Does not implicitly {@link otsimo.UploadReq.verify|verify} messages.
         * @param message UploadReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IUploadReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an UploadReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UploadReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.UploadReq;

        /**
         * Decodes an UploadReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UploadReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.UploadReq;

        /**
         * Verifies an UploadReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an UploadReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns UploadReq
         */
        public static fromObject(object: { [k: string]: any }): otsimo.UploadReq;

        /**
         * Creates a plain object from an UploadReq message. Also converts values to other types if specified.
         * @param message UploadReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.UploadReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this UploadReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an UploadRes. */
    interface IUploadRes {

        /** UploadRes uploadUrls */
        uploadUrls?: { [k: string]: string };
    }

    /** Represents an UploadRes. */
    class UploadRes {

        /**
         * Constructs a new UploadRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IUploadRes);

        /** UploadRes uploadUrls. */
        public uploadUrls: { [k: string]: string };

        /**
         * Creates a new UploadRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UploadRes instance
         */
        public static create(properties?: otsimo.IUploadRes): otsimo.UploadRes;

        /**
         * Encodes the specified UploadRes message. Does not implicitly {@link otsimo.UploadRes.verify|verify} messages.
         * @param message UploadRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IUploadRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified UploadRes message, length delimited. Does not implicitly {@link otsimo.UploadRes.verify|verify} messages.
         * @param message UploadRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IUploadRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an UploadRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UploadRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.UploadRes;

        /**
         * Decodes an UploadRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UploadRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.UploadRes;

        /**
         * Verifies an UploadRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an UploadRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns UploadRes
         */
        public static fromObject(object: { [k: string]: any }): otsimo.UploadRes;

        /**
         * Creates a plain object from an UploadRes message. Also converts values to other types if specified.
         * @param message UploadRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.UploadRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this UploadRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a StoreSmallReq. */
    interface IStoreSmallReq {

        /** StoreSmallReq metadata */
        metadata?: otsimo.IFileMetadata;

        /** StoreSmallReq data */
        data?: Uint8Array;
    }

    /** Represents a StoreSmallReq. */
    class StoreSmallReq {

        /**
         * Constructs a new StoreSmallReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IStoreSmallReq);

        /** StoreSmallReq metadata. */
        public metadata?: (otsimo.IFileMetadata|null);

        /** StoreSmallReq data. */
        public data: Uint8Array;

        /**
         * Creates a new StoreSmallReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StoreSmallReq instance
         */
        public static create(properties?: otsimo.IStoreSmallReq): otsimo.StoreSmallReq;

        /**
         * Encodes the specified StoreSmallReq message. Does not implicitly {@link otsimo.StoreSmallReq.verify|verify} messages.
         * @param message StoreSmallReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IStoreSmallReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified StoreSmallReq message, length delimited. Does not implicitly {@link otsimo.StoreSmallReq.verify|verify} messages.
         * @param message StoreSmallReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IStoreSmallReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a StoreSmallReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StoreSmallReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.StoreSmallReq;

        /**
         * Decodes a StoreSmallReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StoreSmallReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.StoreSmallReq;

        /**
         * Verifies a StoreSmallReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a StoreSmallReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns StoreSmallReq
         */
        public static fromObject(object: { [k: string]: any }): otsimo.StoreSmallReq;

        /**
         * Creates a plain object from a StoreSmallReq message. Also converts values to other types if specified.
         * @param message StoreSmallReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.StoreSmallReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this StoreSmallReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a StoreRes. */
    interface IStoreRes {

        /** StoreRes metadata */
        metadata?: otsimo.IFileMetadata;
    }

    /** Represents a StoreRes. */
    class StoreRes {

        /**
         * Constructs a new StoreRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.IStoreRes);

        /** StoreRes metadata. */
        public metadata?: (otsimo.IFileMetadata|null);

        /**
         * Creates a new StoreRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StoreRes instance
         */
        public static create(properties?: otsimo.IStoreRes): otsimo.StoreRes;

        /**
         * Encodes the specified StoreRes message. Does not implicitly {@link otsimo.StoreRes.verify|verify} messages.
         * @param message StoreRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.IStoreRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified StoreRes message, length delimited. Does not implicitly {@link otsimo.StoreRes.verify|verify} messages.
         * @param message StoreRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.IStoreRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a StoreRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StoreRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.StoreRes;

        /**
         * Decodes a StoreRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StoreRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.StoreRes;

        /**
         * Verifies a StoreRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a StoreRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns StoreRes
         */
        public static fromObject(object: { [k: string]: any }): otsimo.StoreRes;

        /**
         * Creates a plain object from a StoreRes message. Also converts values to other types if specified.
         * @param message StoreRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.StoreRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this StoreRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a LookupReq. */
    interface ILookupReq {

        /** LookupReq selector */
        selector?: apipb.ILabelSelector;
    }

    /** Represents a LookupReq. */
    class LookupReq {

        /**
         * Constructs a new LookupReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.ILookupReq);

        /** LookupReq selector. */
        public selector?: (apipb.ILabelSelector|null);

        /**
         * Creates a new LookupReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LookupReq instance
         */
        public static create(properties?: otsimo.ILookupReq): otsimo.LookupReq;

        /**
         * Encodes the specified LookupReq message. Does not implicitly {@link otsimo.LookupReq.verify|verify} messages.
         * @param message LookupReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.ILookupReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LookupReq message, length delimited. Does not implicitly {@link otsimo.LookupReq.verify|verify} messages.
         * @param message LookupReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.ILookupReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LookupReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LookupReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.LookupReq;

        /**
         * Decodes a LookupReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LookupReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.LookupReq;

        /**
         * Verifies a LookupReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LookupReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LookupReq
         */
        public static fromObject(object: { [k: string]: any }): otsimo.LookupReq;

        /**
         * Creates a plain object from a LookupReq message. Also converts values to other types if specified.
         * @param message LookupReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.LookupReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LookupReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a LookupRes. */
    interface ILookupRes {

        /** LookupRes metadata */
        metadata?: otsimo.IFileMetadata[];
    }

    /** Represents a LookupRes. */
    class LookupRes {

        /**
         * Constructs a new LookupRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: otsimo.ILookupRes);

        /** LookupRes metadata. */
        public metadata: otsimo.IFileMetadata[];

        /**
         * Creates a new LookupRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LookupRes instance
         */
        public static create(properties?: otsimo.ILookupRes): otsimo.LookupRes;

        /**
         * Encodes the specified LookupRes message. Does not implicitly {@link otsimo.LookupRes.verify|verify} messages.
         * @param message LookupRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: otsimo.ILookupRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LookupRes message, length delimited. Does not implicitly {@link otsimo.LookupRes.verify|verify} messages.
         * @param message LookupRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: otsimo.ILookupRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LookupRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LookupRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): otsimo.LookupRes;

        /**
         * Decodes a LookupRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LookupRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): otsimo.LookupRes;

        /**
         * Verifies a LookupRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LookupRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LookupRes
         */
        public static fromObject(object: { [k: string]: any }): otsimo.LookupRes;

        /**
         * Creates a plain object from a LookupRes message. Also converts values to other types if specified.
         * @param message LookupRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: otsimo.LookupRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LookupRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}

/** Namespace apipb. */
export namespace apipb {

    /** RequestReleaseState enum. */
    enum RequestReleaseState {
        PRODUCTION_STATE = 0,
        ALL_STATES = 1
    }

    /** Properties of a GetProfileRequest. */
    interface IGetProfileRequest {

        /** GetProfileRequest id */
        id?: string;

        /** GetProfileRequest email */
        email?: string;
    }

    /** Represents a GetProfileRequest. */
    class GetProfileRequest {

        /**
         * Constructs a new GetProfileRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGetProfileRequest);

        /** GetProfileRequest id. */
        public id: string;

        /** GetProfileRequest email. */
        public email: string;

        /**
         * Creates a new GetProfileRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetProfileRequest instance
         */
        public static create(properties?: apipb.IGetProfileRequest): apipb.GetProfileRequest;

        /**
         * Encodes the specified GetProfileRequest message. Does not implicitly {@link apipb.GetProfileRequest.verify|verify} messages.
         * @param message GetProfileRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGetProfileRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetProfileRequest message, length delimited. Does not implicitly {@link apipb.GetProfileRequest.verify|verify} messages.
         * @param message GetProfileRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGetProfileRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetProfileRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetProfileRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GetProfileRequest;

        /**
         * Decodes a GetProfileRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetProfileRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GetProfileRequest;

        /**
         * Verifies a GetProfileRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetProfileRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetProfileRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GetProfileRequest;

        /**
         * Creates a plain object from a GetProfileRequest message. Also converts values to other types if specified.
         * @param message GetProfileRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GetProfileRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetProfileRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GetChildRequest. */
    interface IGetChildRequest {

        /** GetChildRequest childId */
        childId?: string;
    }

    /** Represents a GetChildRequest. */
    class GetChildRequest {

        /**
         * Constructs a new GetChildRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGetChildRequest);

        /** GetChildRequest childId. */
        public childId: string;

        /**
         * Creates a new GetChildRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetChildRequest instance
         */
        public static create(properties?: apipb.IGetChildRequest): apipb.GetChildRequest;

        /**
         * Encodes the specified GetChildRequest message. Does not implicitly {@link apipb.GetChildRequest.verify|verify} messages.
         * @param message GetChildRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGetChildRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetChildRequest message, length delimited. Does not implicitly {@link apipb.GetChildRequest.verify|verify} messages.
         * @param message GetChildRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGetChildRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetChildRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetChildRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GetChildRequest;

        /**
         * Decodes a GetChildRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetChildRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GetChildRequest;

        /**
         * Verifies a GetChildRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetChildRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetChildRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GetChildRequest;

        /**
         * Creates a plain object from a GetChildRequest message. Also converts values to other types if specified.
         * @param message GetChildRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GetChildRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetChildRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GetChildrenFromProfileRequest. */
    interface IGetChildrenFromProfileRequest {

        /** GetChildrenFromProfileRequest profileId */
        profileId?: string;
    }

    /** Represents a GetChildrenFromProfileRequest. */
    class GetChildrenFromProfileRequest {

        /**
         * Constructs a new GetChildrenFromProfileRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGetChildrenFromProfileRequest);

        /** GetChildrenFromProfileRequest profileId. */
        public profileId: string;

        /**
         * Creates a new GetChildrenFromProfileRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetChildrenFromProfileRequest instance
         */
        public static create(properties?: apipb.IGetChildrenFromProfileRequest): apipb.GetChildrenFromProfileRequest;

        /**
         * Encodes the specified GetChildrenFromProfileRequest message. Does not implicitly {@link apipb.GetChildrenFromProfileRequest.verify|verify} messages.
         * @param message GetChildrenFromProfileRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGetChildrenFromProfileRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetChildrenFromProfileRequest message, length delimited. Does not implicitly {@link apipb.GetChildrenFromProfileRequest.verify|verify} messages.
         * @param message GetChildrenFromProfileRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGetChildrenFromProfileRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetChildrenFromProfileRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetChildrenFromProfileRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GetChildrenFromProfileRequest;

        /**
         * Decodes a GetChildrenFromProfileRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetChildrenFromProfileRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GetChildrenFromProfileRequest;

        /**
         * Verifies a GetChildrenFromProfileRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetChildrenFromProfileRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetChildrenFromProfileRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GetChildrenFromProfileRequest;

        /**
         * Creates a plain object from a GetChildrenFromProfileRequest message. Also converts values to other types if specified.
         * @param message GetChildrenFromProfileRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GetChildrenFromProfileRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetChildrenFromProfileRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a ChangeChildActivationRequest. */
    interface IChangeChildActivationRequest {

        /** ChangeChildActivationRequest childId */
        childId?: string;

        /** ChangeChildActivationRequest active */
        active?: boolean;
    }

    /** Represents a ChangeChildActivationRequest. */
    class ChangeChildActivationRequest {

        /**
         * Constructs a new ChangeChildActivationRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IChangeChildActivationRequest);

        /** ChangeChildActivationRequest childId. */
        public childId: string;

        /** ChangeChildActivationRequest active. */
        public active: boolean;

        /**
         * Creates a new ChangeChildActivationRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ChangeChildActivationRequest instance
         */
        public static create(properties?: apipb.IChangeChildActivationRequest): apipb.ChangeChildActivationRequest;

        /**
         * Encodes the specified ChangeChildActivationRequest message. Does not implicitly {@link apipb.ChangeChildActivationRequest.verify|verify} messages.
         * @param message ChangeChildActivationRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IChangeChildActivationRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ChangeChildActivationRequest message, length delimited. Does not implicitly {@link apipb.ChangeChildActivationRequest.verify|verify} messages.
         * @param message ChangeChildActivationRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IChangeChildActivationRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ChangeChildActivationRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ChangeChildActivationRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.ChangeChildActivationRequest;

        /**
         * Decodes a ChangeChildActivationRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ChangeChildActivationRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.ChangeChildActivationRequest;

        /**
         * Verifies a ChangeChildActivationRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ChangeChildActivationRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ChangeChildActivationRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.ChangeChildActivationRequest;

        /**
         * Creates a plain object from a ChangeChildActivationRequest message. Also converts values to other types if specified.
         * @param message ChangeChildActivationRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.ChangeChildActivationRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ChangeChildActivationRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GetChildrenFromProfileResponse. */
    interface IGetChildrenFromProfileResponse {

        /** GetChildrenFromProfileResponse children */
        children?: apipb.IChild[];
    }

    /** Represents a GetChildrenFromProfileResponse. */
    class GetChildrenFromProfileResponse {

        /**
         * Constructs a new GetChildrenFromProfileResponse.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGetChildrenFromProfileResponse);

        /** GetChildrenFromProfileResponse children. */
        public children: apipb.IChild[];

        /**
         * Creates a new GetChildrenFromProfileResponse instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetChildrenFromProfileResponse instance
         */
        public static create(properties?: apipb.IGetChildrenFromProfileResponse): apipb.GetChildrenFromProfileResponse;

        /**
         * Encodes the specified GetChildrenFromProfileResponse message. Does not implicitly {@link apipb.GetChildrenFromProfileResponse.verify|verify} messages.
         * @param message GetChildrenFromProfileResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGetChildrenFromProfileResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetChildrenFromProfileResponse message, length delimited. Does not implicitly {@link apipb.GetChildrenFromProfileResponse.verify|verify} messages.
         * @param message GetChildrenFromProfileResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGetChildrenFromProfileResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetChildrenFromProfileResponse message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetChildrenFromProfileResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GetChildrenFromProfileResponse;

        /**
         * Decodes a GetChildrenFromProfileResponse message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetChildrenFromProfileResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GetChildrenFromProfileResponse;

        /**
         * Verifies a GetChildrenFromProfileResponse message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetChildrenFromProfileResponse message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetChildrenFromProfileResponse
         */
        public static fromObject(object: { [k: string]: any }): apipb.GetChildrenFromProfileResponse;

        /**
         * Creates a plain object from a GetChildrenFromProfileResponse message. Also converts values to other types if specified.
         * @param message GetChildrenFromProfileResponse
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GetChildrenFromProfileResponse, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetChildrenFromProfileResponse to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GetGameReleaseRequest. */
    interface IGetGameReleaseRequest {

        /** GetGameReleaseRequest gameId */
        gameId?: string;

        /** GetGameReleaseRequest version */
        version?: string;

        /** GetGameReleaseRequest state */
        state?: apipb.RequestReleaseState;
    }

    /** Represents a GetGameReleaseRequest. */
    class GetGameReleaseRequest {

        /**
         * Constructs a new GetGameReleaseRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGetGameReleaseRequest);

        /** GetGameReleaseRequest gameId. */
        public gameId: string;

        /** GetGameReleaseRequest version. */
        public version: string;

        /** GetGameReleaseRequest state. */
        public state: apipb.RequestReleaseState;

        /**
         * Creates a new GetGameReleaseRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetGameReleaseRequest instance
         */
        public static create(properties?: apipb.IGetGameReleaseRequest): apipb.GetGameReleaseRequest;

        /**
         * Encodes the specified GetGameReleaseRequest message. Does not implicitly {@link apipb.GetGameReleaseRequest.verify|verify} messages.
         * @param message GetGameReleaseRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGetGameReleaseRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetGameReleaseRequest message, length delimited. Does not implicitly {@link apipb.GetGameReleaseRequest.verify|verify} messages.
         * @param message GetGameReleaseRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGetGameReleaseRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetGameReleaseRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetGameReleaseRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GetGameReleaseRequest;

        /**
         * Decodes a GetGameReleaseRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetGameReleaseRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GetGameReleaseRequest;

        /**
         * Verifies a GetGameReleaseRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetGameReleaseRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetGameReleaseRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GetGameReleaseRequest;

        /**
         * Creates a plain object from a GetGameReleaseRequest message. Also converts values to other types if specified.
         * @param message GetGameReleaseRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GetGameReleaseRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetGameReleaseRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SoundEnableRequest. */
    interface ISoundEnableRequest {

        /** SoundEnableRequest childId */
        childId?: string;

        /** SoundEnableRequest profileId */
        profileId?: string;

        /** SoundEnableRequest enable */
        enable?: boolean;
    }

    /** Represents a SoundEnableRequest. */
    class SoundEnableRequest {

        /**
         * Constructs a new SoundEnableRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.ISoundEnableRequest);

        /** SoundEnableRequest childId. */
        public childId: string;

        /** SoundEnableRequest profileId. */
        public profileId: string;

        /** SoundEnableRequest enable. */
        public enable: boolean;

        /**
         * Creates a new SoundEnableRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SoundEnableRequest instance
         */
        public static create(properties?: apipb.ISoundEnableRequest): apipb.SoundEnableRequest;

        /**
         * Encodes the specified SoundEnableRequest message. Does not implicitly {@link apipb.SoundEnableRequest.verify|verify} messages.
         * @param message SoundEnableRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.ISoundEnableRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SoundEnableRequest message, length delimited. Does not implicitly {@link apipb.SoundEnableRequest.verify|verify} messages.
         * @param message SoundEnableRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.ISoundEnableRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SoundEnableRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SoundEnableRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.SoundEnableRequest;

        /**
         * Decodes a SoundEnableRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SoundEnableRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.SoundEnableRequest;

        /**
         * Verifies a SoundEnableRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SoundEnableRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SoundEnableRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.SoundEnableRequest;

        /**
         * Creates a plain object from a SoundEnableRequest message. Also converts values to other types if specified.
         * @param message SoundEnableRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.SoundEnableRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SoundEnableRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GameEntryRequest. */
    interface IGameEntryRequest {

        /** GameEntryRequest childId */
        childId?: string;

        /** GameEntryRequest gameId */
        gameId?: string;

        /** GameEntryRequest type */
        type?: apipb.GameEntryRequest.RequestType;

        /** GameEntryRequest settings */
        settings?: Uint8Array;

        /** GameEntryRequest index */
        index?: number;
    }

    /** Represents a GameEntryRequest. */
    class GameEntryRequest {

        /**
         * Constructs a new GameEntryRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGameEntryRequest);

        /** GameEntryRequest childId. */
        public childId: string;

        /** GameEntryRequest gameId. */
        public gameId: string;

        /** GameEntryRequest type. */
        public type: apipb.GameEntryRequest.RequestType;

        /** GameEntryRequest settings. */
        public settings: Uint8Array;

        /** GameEntryRequest index. */
        public index: number;

        /**
         * Creates a new GameEntryRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameEntryRequest instance
         */
        public static create(properties?: apipb.IGameEntryRequest): apipb.GameEntryRequest;

        /**
         * Encodes the specified GameEntryRequest message. Does not implicitly {@link apipb.GameEntryRequest.verify|verify} messages.
         * @param message GameEntryRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGameEntryRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GameEntryRequest message, length delimited. Does not implicitly {@link apipb.GameEntryRequest.verify|verify} messages.
         * @param message GameEntryRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGameEntryRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GameEntryRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameEntryRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GameEntryRequest;

        /**
         * Decodes a GameEntryRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameEntryRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GameEntryRequest;

        /**
         * Verifies a GameEntryRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GameEntryRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GameEntryRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GameEntryRequest;

        /**
         * Creates a plain object from a GameEntryRequest message. Also converts values to other types if specified.
         * @param message GameEntryRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GameEntryRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GameEntryRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace GameEntryRequest {

        /** RequestType enum. */
        enum RequestType {
            ADD = 0,
            ACTIVATE = 1,
            DEACTIVATE = 2,
            SETTINGS = 3,
            INDEX = 4,
            LOCALSETTINGS = 5
        }
    }

    /** Properties of a PublishResponse. */
    interface IPublishResponse {

        /** PublishResponse type */
        type?: number;

        /** PublishResponse message */
        message?: string;

        /** PublishResponse token */
        token?: apipb.IUploadToken;
    }

    /** Represents a PublishResponse. */
    class PublishResponse {

        /**
         * Constructs a new PublishResponse.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IPublishResponse);

        /** PublishResponse type. */
        public type: number;

        /** PublishResponse message. */
        public message: string;

        /** PublishResponse token. */
        public token?: (apipb.IUploadToken|null);

        /**
         * Creates a new PublishResponse instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PublishResponse instance
         */
        public static create(properties?: apipb.IPublishResponse): apipb.PublishResponse;

        /**
         * Encodes the specified PublishResponse message. Does not implicitly {@link apipb.PublishResponse.verify|verify} messages.
         * @param message PublishResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IPublishResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified PublishResponse message, length delimited. Does not implicitly {@link apipb.PublishResponse.verify|verify} messages.
         * @param message PublishResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IPublishResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PublishResponse message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PublishResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.PublishResponse;

        /**
         * Decodes a PublishResponse message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PublishResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.PublishResponse;

        /**
         * Verifies a PublishResponse message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a PublishResponse message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns PublishResponse
         */
        public static fromObject(object: { [k: string]: any }): apipb.PublishResponse;

        /**
         * Creates a plain object from a PublishResponse message. Also converts values to other types if specified.
         * @param message PublishResponse
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.PublishResponse, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this PublishResponse to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a ValidateRequest. */
    interface IValidateRequest {

        /** ValidateRequest gameId */
        gameId?: string;

        /** ValidateRequest gameVersion */
        gameVersion?: string;

        /** ValidateRequest newState */
        newState?: apipb.ReleaseState;
    }

    /** Represents a ValidateRequest. */
    class ValidateRequest {

        /**
         * Constructs a new ValidateRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IValidateRequest);

        /** ValidateRequest gameId. */
        public gameId: string;

        /** ValidateRequest gameVersion. */
        public gameVersion: string;

        /** ValidateRequest newState. */
        public newState: apipb.ReleaseState;

        /**
         * Creates a new ValidateRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ValidateRequest instance
         */
        public static create(properties?: apipb.IValidateRequest): apipb.ValidateRequest;

        /**
         * Encodes the specified ValidateRequest message. Does not implicitly {@link apipb.ValidateRequest.verify|verify} messages.
         * @param message ValidateRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IValidateRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ValidateRequest message, length delimited. Does not implicitly {@link apipb.ValidateRequest.verify|verify} messages.
         * @param message ValidateRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IValidateRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ValidateRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ValidateRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.ValidateRequest;

        /**
         * Decodes a ValidateRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ValidateRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.ValidateRequest;

        /**
         * Verifies a ValidateRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ValidateRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ValidateRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.ValidateRequest;

        /**
         * Creates a plain object from a ValidateRequest message. Also converts values to other types if specified.
         * @param message ValidateRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.ValidateRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ValidateRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an UpdateIndecesRequest. */
    interface IUpdateIndecesRequest {

        /** UpdateIndecesRequest profileId */
        profileId?: string;

        /** UpdateIndecesRequest childId */
        childId?: string;

        /** UpdateIndecesRequest gameIds */
        gameIds?: string[];
    }

    /** Represents an UpdateIndecesRequest. */
    class UpdateIndecesRequest {

        /**
         * Constructs a new UpdateIndecesRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IUpdateIndecesRequest);

        /** UpdateIndecesRequest profileId. */
        public profileId: string;

        /** UpdateIndecesRequest childId. */
        public childId: string;

        /** UpdateIndecesRequest gameIds. */
        public gameIds: string[];

        /**
         * Creates a new UpdateIndecesRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UpdateIndecesRequest instance
         */
        public static create(properties?: apipb.IUpdateIndecesRequest): apipb.UpdateIndecesRequest;

        /**
         * Encodes the specified UpdateIndecesRequest message. Does not implicitly {@link apipb.UpdateIndecesRequest.verify|verify} messages.
         * @param message UpdateIndecesRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IUpdateIndecesRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified UpdateIndecesRequest message, length delimited. Does not implicitly {@link apipb.UpdateIndecesRequest.verify|verify} messages.
         * @param message UpdateIndecesRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IUpdateIndecesRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an UpdateIndecesRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UpdateIndecesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.UpdateIndecesRequest;

        /**
         * Decodes an UpdateIndecesRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UpdateIndecesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.UpdateIndecesRequest;

        /**
         * Verifies an UpdateIndecesRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an UpdateIndecesRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns UpdateIndecesRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.UpdateIndecesRequest;

        /**
         * Creates a plain object from an UpdateIndecesRequest message. Also converts values to other types if specified.
         * @param message UpdateIndecesRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.UpdateIndecesRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this UpdateIndecesRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GetGameRequest. */
    interface IGetGameRequest {

        /** GetGameRequest uniqueName */
        uniqueName?: string;

        /** GetGameRequest gameId */
        gameId?: string;
    }

    /** Represents a GetGameRequest. */
    class GetGameRequest {

        /**
         * Constructs a new GetGameRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGetGameRequest);

        /** GetGameRequest uniqueName. */
        public uniqueName: string;

        /** GetGameRequest gameId. */
        public gameId: string;

        /**
         * Creates a new GetGameRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetGameRequest instance
         */
        public static create(properties?: apipb.IGetGameRequest): apipb.GetGameRequest;

        /**
         * Encodes the specified GetGameRequest message. Does not implicitly {@link apipb.GetGameRequest.verify|verify} messages.
         * @param message GetGameRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGetGameRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetGameRequest message, length delimited. Does not implicitly {@link apipb.GetGameRequest.verify|verify} messages.
         * @param message GetGameRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGetGameRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetGameRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetGameRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GetGameRequest;

        /**
         * Decodes a GetGameRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetGameRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GetGameRequest;

        /**
         * Verifies a GetGameRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetGameRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetGameRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GetGameRequest;

        /**
         * Creates a plain object from a GetGameRequest message. Also converts values to other types if specified.
         * @param message GetGameRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GetGameRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetGameRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a ListGamesRequest. */
    interface IListGamesRequest {

        /** ListGamesRequest releaseState */
        releaseState?: apipb.ListGamesRequest.InnerState;

        /** ListGamesRequest limit */
        limit?: number;

        /** ListGamesRequest offset */
        offset?: number;

        /** ListGamesRequest language */
        language?: string;
    }

    /** Represents a ListGamesRequest. */
    class ListGamesRequest {

        /**
         * Constructs a new ListGamesRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IListGamesRequest);

        /** ListGamesRequest releaseState. */
        public releaseState: apipb.ListGamesRequest.InnerState;

        /** ListGamesRequest limit. */
        public limit: number;

        /** ListGamesRequest offset. */
        public offset: number;

        /** ListGamesRequest language. */
        public language: string;

        /**
         * Creates a new ListGamesRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ListGamesRequest instance
         */
        public static create(properties?: apipb.IListGamesRequest): apipb.ListGamesRequest;

        /**
         * Encodes the specified ListGamesRequest message. Does not implicitly {@link apipb.ListGamesRequest.verify|verify} messages.
         * @param message ListGamesRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IListGamesRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ListGamesRequest message, length delimited. Does not implicitly {@link apipb.ListGamesRequest.verify|verify} messages.
         * @param message ListGamesRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IListGamesRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ListGamesRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ListGamesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.ListGamesRequest;

        /**
         * Decodes a ListGamesRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ListGamesRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.ListGamesRequest;

        /**
         * Verifies a ListGamesRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ListGamesRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ListGamesRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.ListGamesRequest;

        /**
         * Creates a plain object from a ListGamesRequest message. Also converts values to other types if specified.
         * @param message ListGamesRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.ListGamesRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ListGamesRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace ListGamesRequest {

        /** InnerState enum. */
        enum InnerState {
            ANY = 0,
            CREATED = 1,
            DEVELOPMENT = 2,
            WAITING = 3,
            REJECTED = 4,
            VALIDATED = 5,
            PRODUCTION = 6
        }
    }

    /** Properties of a ListItem. */
    interface IListItem {

        /** ListItem gameId */
        gameId?: string;

        /** ListItem uniqueName */
        uniqueName?: string;

        /** ListItem latestVersion */
        latestVersion?: string;

        /** ListItem latestState */
        latestState?: apipb.ReleaseState;

        /** ListItem productionVersion */
        productionVersion?: string;

        /** ListItem storage */
        storage?: string;

        /** ListItem archiveFormat */
        archiveFormat?: string;

        /** ListItem releasedAt */
        releasedAt?: (number|Long);

        /** ListItem languages */
        languages?: string[];
    }

    /** Represents a ListItem. */
    class ListItem {

        /**
         * Constructs a new ListItem.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IListItem);

        /** ListItem gameId. */
        public gameId: string;

        /** ListItem uniqueName. */
        public uniqueName: string;

        /** ListItem latestVersion. */
        public latestVersion: string;

        /** ListItem latestState. */
        public latestState: apipb.ReleaseState;

        /** ListItem productionVersion. */
        public productionVersion: string;

        /** ListItem storage. */
        public storage: string;

        /** ListItem archiveFormat. */
        public archiveFormat: string;

        /** ListItem releasedAt. */
        public releasedAt: (number|Long);

        /** ListItem languages. */
        public languages: string[];

        /**
         * Creates a new ListItem instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ListItem instance
         */
        public static create(properties?: apipb.IListItem): apipb.ListItem;

        /**
         * Encodes the specified ListItem message. Does not implicitly {@link apipb.ListItem.verify|verify} messages.
         * @param message ListItem message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IListItem, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ListItem message, length delimited. Does not implicitly {@link apipb.ListItem.verify|verify} messages.
         * @param message ListItem message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IListItem, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ListItem message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ListItem
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.ListItem;

        /**
         * Decodes a ListItem message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ListItem
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.ListItem;

        /**
         * Verifies a ListItem message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ListItem message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ListItem
         */
        public static fromObject(object: { [k: string]: any }): apipb.ListItem;

        /**
         * Creates a plain object from a ListItem message. Also converts values to other types if specified.
         * @param message ListItem
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.ListItem, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ListItem to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a ListItemWithTests. */
    interface IListItemWithTests {

        /** ListItemWithTests gameId */
        gameId?: string;

        /** ListItemWithTests uniqueName */
        uniqueName?: string;

        /** ListItemWithTests testingVersion */
        testingVersion?: number;

        /** ListItemWithTests groups */
        groups?: apipb.ListItemWithTests.ITestGroup[];
    }

    /** Represents a ListItemWithTests. */
    class ListItemWithTests {

        /**
         * Constructs a new ListItemWithTests.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IListItemWithTests);

        /** ListItemWithTests gameId. */
        public gameId: string;

        /** ListItemWithTests uniqueName. */
        public uniqueName: string;

        /** ListItemWithTests testingVersion. */
        public testingVersion: number;

        /** ListItemWithTests groups. */
        public groups: apipb.ListItemWithTests.ITestGroup[];

        /**
         * Creates a new ListItemWithTests instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ListItemWithTests instance
         */
        public static create(properties?: apipb.IListItemWithTests): apipb.ListItemWithTests;

        /**
         * Encodes the specified ListItemWithTests message. Does not implicitly {@link apipb.ListItemWithTests.verify|verify} messages.
         * @param message ListItemWithTests message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IListItemWithTests, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ListItemWithTests message, length delimited. Does not implicitly {@link apipb.ListItemWithTests.verify|verify} messages.
         * @param message ListItemWithTests message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IListItemWithTests, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ListItemWithTests message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ListItemWithTests
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.ListItemWithTests;

        /**
         * Decodes a ListItemWithTests message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ListItemWithTests
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.ListItemWithTests;

        /**
         * Verifies a ListItemWithTests message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ListItemWithTests message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ListItemWithTests
         */
        public static fromObject(object: { [k: string]: any }): apipb.ListItemWithTests;

        /**
         * Creates a plain object from a ListItemWithTests message. Also converts values to other types if specified.
         * @param message ListItemWithTests
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.ListItemWithTests, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ListItemWithTests to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace ListItemWithTests {

        /** Properties of a TestGroup. */
        interface ITestGroup {

            /** TestGroup name */
            name?: string;

            /** TestGroup weight */
            weight?: number;

            /** TestGroup latestVersion */
            latestVersion?: string;

            /** TestGroup latestState */
            latestState?: apipb.ReleaseState;

            /** TestGroup productionVersion */
            productionVersion?: string;

            /** TestGroup storage */
            storage?: string;

            /** TestGroup archiveFormat */
            archiveFormat?: string;

            /** TestGroup releasedAt */
            releasedAt?: (number|Long);

            /** TestGroup languages */
            languages?: string[];
        }

        /** Represents a TestGroup. */
        class TestGroup {

            /**
             * Constructs a new TestGroup.
             * @param [properties] Properties to set
             */
            constructor(properties?: apipb.ListItemWithTests.ITestGroup);

            /** TestGroup name. */
            public name: string;

            /** TestGroup weight. */
            public weight: number;

            /** TestGroup latestVersion. */
            public latestVersion: string;

            /** TestGroup latestState. */
            public latestState: apipb.ReleaseState;

            /** TestGroup productionVersion. */
            public productionVersion: string;

            /** TestGroup storage. */
            public storage: string;

            /** TestGroup archiveFormat. */
            public archiveFormat: string;

            /** TestGroup releasedAt. */
            public releasedAt: (number|Long);

            /** TestGroup languages. */
            public languages: string[];

            /**
             * Creates a new TestGroup instance using the specified properties.
             * @param [properties] Properties to set
             * @returns TestGroup instance
             */
            public static create(properties?: apipb.ListItemWithTests.ITestGroup): apipb.ListItemWithTests.TestGroup;

            /**
             * Encodes the specified TestGroup message. Does not implicitly {@link apipb.ListItemWithTests.TestGroup.verify|verify} messages.
             * @param message TestGroup message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: apipb.ListItemWithTests.ITestGroup, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified TestGroup message, length delimited. Does not implicitly {@link apipb.ListItemWithTests.TestGroup.verify|verify} messages.
             * @param message TestGroup message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: apipb.ListItemWithTests.ITestGroup, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a TestGroup message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns TestGroup
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.ListItemWithTests.TestGroup;

            /**
             * Decodes a TestGroup message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns TestGroup
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.ListItemWithTests.TestGroup;

            /**
             * Verifies a TestGroup message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a TestGroup message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns TestGroup
             */
            public static fromObject(object: { [k: string]: any }): apipb.ListItemWithTests.TestGroup;

            /**
             * Creates a plain object from a TestGroup message. Also converts values to other types if specified.
             * @param message TestGroup
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: apipb.ListItemWithTests.TestGroup, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this TestGroup to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }

    /** Properties of a GetLatestVersionsRequest. */
    interface IGetLatestVersionsRequest {

        /** GetLatestVersionsRequest state */
        state?: apipb.RequestReleaseState;

        /** GetLatestVersionsRequest gameIds */
        gameIds?: string[];

        /** GetLatestVersionsRequest capabilities */
        capabilities?: string[];
    }

    /** Represents a GetLatestVersionsRequest. */
    class GetLatestVersionsRequest {

        /**
         * Constructs a new GetLatestVersionsRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGetLatestVersionsRequest);

        /** GetLatestVersionsRequest state. */
        public state: apipb.RequestReleaseState;

        /** GetLatestVersionsRequest gameIds. */
        public gameIds: string[];

        /** GetLatestVersionsRequest capabilities. */
        public capabilities: string[];

        /**
         * Creates a new GetLatestVersionsRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetLatestVersionsRequest instance
         */
        public static create(properties?: apipb.IGetLatestVersionsRequest): apipb.GetLatestVersionsRequest;

        /**
         * Encodes the specified GetLatestVersionsRequest message. Does not implicitly {@link apipb.GetLatestVersionsRequest.verify|verify} messages.
         * @param message GetLatestVersionsRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGetLatestVersionsRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetLatestVersionsRequest message, length delimited. Does not implicitly {@link apipb.GetLatestVersionsRequest.verify|verify} messages.
         * @param message GetLatestVersionsRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGetLatestVersionsRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetLatestVersionsRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetLatestVersionsRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GetLatestVersionsRequest;

        /**
         * Decodes a GetLatestVersionsRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetLatestVersionsRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GetLatestVersionsRequest;

        /**
         * Verifies a GetLatestVersionsRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetLatestVersionsRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetLatestVersionsRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GetLatestVersionsRequest;

        /**
         * Creates a plain object from a GetLatestVersionsRequest message. Also converts values to other types if specified.
         * @param message GetLatestVersionsRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GetLatestVersionsRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetLatestVersionsRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GameAndVersion. */
    interface IGameAndVersion {

        /** GameAndVersion gameId */
        gameId?: string;

        /** GameAndVersion version */
        version?: string;

        /** GameAndVersion tarballUrl */
        tarballUrl?: string;
    }

    /** Represents a GameAndVersion. */
    class GameAndVersion {

        /**
         * Constructs a new GameAndVersion.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGameAndVersion);

        /** GameAndVersion gameId. */
        public gameId: string;

        /** GameAndVersion version. */
        public version: string;

        /** GameAndVersion tarballUrl. */
        public tarballUrl: string;

        /**
         * Creates a new GameAndVersion instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameAndVersion instance
         */
        public static create(properties?: apipb.IGameAndVersion): apipb.GameAndVersion;

        /**
         * Encodes the specified GameAndVersion message. Does not implicitly {@link apipb.GameAndVersion.verify|verify} messages.
         * @param message GameAndVersion message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGameAndVersion, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GameAndVersion message, length delimited. Does not implicitly {@link apipb.GameAndVersion.verify|verify} messages.
         * @param message GameAndVersion message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGameAndVersion, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GameAndVersion message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameAndVersion
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GameAndVersion;

        /**
         * Decodes a GameAndVersion message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameAndVersion
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GameAndVersion;

        /**
         * Verifies a GameAndVersion message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GameAndVersion message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GameAndVersion
         */
        public static fromObject(object: { [k: string]: any }): apipb.GameAndVersion;

        /**
         * Creates a plain object from a GameAndVersion message. Also converts values to other types if specified.
         * @param message GameAndVersion
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GameAndVersion, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GameAndVersion to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GameVersionsResponse. */
    interface IGameVersionsResponse {

        /** GameVersionsResponse results */
        results?: apipb.IGameAndVersion[];
    }

    /** Represents a GameVersionsResponse. */
    class GameVersionsResponse {

        /**
         * Constructs a new GameVersionsResponse.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGameVersionsResponse);

        /** GameVersionsResponse results. */
        public results: apipb.IGameAndVersion[];

        /**
         * Creates a new GameVersionsResponse instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameVersionsResponse instance
         */
        public static create(properties?: apipb.IGameVersionsResponse): apipb.GameVersionsResponse;

        /**
         * Encodes the specified GameVersionsResponse message. Does not implicitly {@link apipb.GameVersionsResponse.verify|verify} messages.
         * @param message GameVersionsResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGameVersionsResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GameVersionsResponse message, length delimited. Does not implicitly {@link apipb.GameVersionsResponse.verify|verify} messages.
         * @param message GameVersionsResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGameVersionsResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GameVersionsResponse message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameVersionsResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GameVersionsResponse;

        /**
         * Decodes a GameVersionsResponse message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameVersionsResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GameVersionsResponse;

        /**
         * Verifies a GameVersionsResponse message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GameVersionsResponse message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GameVersionsResponse
         */
        public static fromObject(object: { [k: string]: any }): apipb.GameVersionsResponse;

        /**
         * Creates a plain object from a GameVersionsResponse message. Also converts values to other types if specified.
         * @param message GameVersionsResponse
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GameVersionsResponse, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GameVersionsResponse to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an IndexRequest. */
    interface IIndexRequest {
    }

    /** Represents an IndexRequest. */
    class IndexRequest {

        /**
         * Constructs a new IndexRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IIndexRequest);

        /**
         * Creates a new IndexRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns IndexRequest instance
         */
        public static create(properties?: apipb.IIndexRequest): apipb.IndexRequest;

        /**
         * Encodes the specified IndexRequest message. Does not implicitly {@link apipb.IndexRequest.verify|verify} messages.
         * @param message IndexRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IIndexRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified IndexRequest message, length delimited. Does not implicitly {@link apipb.IndexRequest.verify|verify} messages.
         * @param message IndexRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IIndexRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an IndexRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns IndexRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.IndexRequest;

        /**
         * Decodes an IndexRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns IndexRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.IndexRequest;

        /**
         * Verifies an IndexRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an IndexRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns IndexRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.IndexRequest;

        /**
         * Creates a plain object from an IndexRequest message. Also converts values to other types if specified.
         * @param message IndexRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.IndexRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this IndexRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SearchResult. */
    interface ISearchResult {

        /** SearchResult gameId */
        gameId?: string;

        /** SearchResult score */
        score?: number;
    }

    /** Represents a SearchResult. */
    class SearchResult {

        /**
         * Constructs a new SearchResult.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.ISearchResult);

        /** SearchResult gameId. */
        public gameId: string;

        /** SearchResult score. */
        public score: number;

        /**
         * Creates a new SearchResult instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SearchResult instance
         */
        public static create(properties?: apipb.ISearchResult): apipb.SearchResult;

        /**
         * Encodes the specified SearchResult message. Does not implicitly {@link apipb.SearchResult.verify|verify} messages.
         * @param message SearchResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.ISearchResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SearchResult message, length delimited. Does not implicitly {@link apipb.SearchResult.verify|verify} messages.
         * @param message SearchResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.ISearchResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SearchResult message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SearchResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.SearchResult;

        /**
         * Decodes a SearchResult message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SearchResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.SearchResult;

        /**
         * Verifies a SearchResult message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SearchResult message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SearchResult
         */
        public static fromObject(object: { [k: string]: any }): apipb.SearchResult;

        /**
         * Creates a plain object from a SearchResult message. Also converts values to other types if specified.
         * @param message SearchResult
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.SearchResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SearchResult to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SearchRequest. */
    interface ISearchRequest {

        /** SearchRequest query */
        query?: string;

        /** SearchRequest state */
        state?: apipb.RequestReleaseState;
    }

    /** Represents a SearchRequest. */
    class SearchRequest {

        /**
         * Constructs a new SearchRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.ISearchRequest);

        /** SearchRequest query. */
        public query: string;

        /** SearchRequest state. */
        public state: apipb.RequestReleaseState;

        /**
         * Creates a new SearchRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SearchRequest instance
         */
        public static create(properties?: apipb.ISearchRequest): apipb.SearchRequest;

        /**
         * Encodes the specified SearchRequest message. Does not implicitly {@link apipb.SearchRequest.verify|verify} messages.
         * @param message SearchRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.ISearchRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SearchRequest message, length delimited. Does not implicitly {@link apipb.SearchRequest.verify|verify} messages.
         * @param message SearchRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.ISearchRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SearchRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SearchRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.SearchRequest;

        /**
         * Decodes a SearchRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SearchRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.SearchRequest;

        /**
         * Verifies a SearchRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SearchRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SearchRequest
         */
        public static fromObject(object: { [k: string]: any }): apipb.SearchRequest;

        /**
         * Creates a plain object from a SearchRequest message. Also converts values to other types if specified.
         * @param message SearchRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.SearchRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SearchRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SearchResponse. */
    interface ISearchResponse {

        /** SearchResponse type */
        type?: number;

        /** SearchResponse results */
        results?: apipb.ISearchResult[];
    }

    /** Represents a SearchResponse. */
    class SearchResponse {

        /**
         * Constructs a new SearchResponse.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.ISearchResponse);

        /** SearchResponse type. */
        public type: number;

        /** SearchResponse results. */
        public results: apipb.ISearchResult[];

        /**
         * Creates a new SearchResponse instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SearchResponse instance
         */
        public static create(properties?: apipb.ISearchResponse): apipb.SearchResponse;

        /**
         * Encodes the specified SearchResponse message. Does not implicitly {@link apipb.SearchResponse.verify|verify} messages.
         * @param message SearchResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.ISearchResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SearchResponse message, length delimited. Does not implicitly {@link apipb.SearchResponse.verify|verify} messages.
         * @param message SearchResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.ISearchResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SearchResponse message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SearchResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.SearchResponse;

        /**
         * Decodes a SearchResponse message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SearchResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.SearchResponse;

        /**
         * Verifies a SearchResponse message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SearchResponse message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SearchResponse
         */
        public static fromObject(object: { [k: string]: any }): apipb.SearchResponse;

        /**
         * Creates a plain object from a SearchResponse message. Also converts values to other types if specified.
         * @param message SearchResponse
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.SearchResponse, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SearchResponse to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Response. */
    interface IResponse {

        /** Response type */
        type?: number;

        /** Response message */
        message?: string;
    }

    /** Represents a Response. */
    class Response {

        /**
         * Constructs a new Response.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IResponse);

        /** Response type. */
        public type: number;

        /** Response message. */
        public message: string;

        /**
         * Creates a new Response instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Response instance
         */
        public static create(properties?: apipb.IResponse): apipb.Response;

        /**
         * Encodes the specified Response message. Does not implicitly {@link apipb.Response.verify|verify} messages.
         * @param message Response message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Response message, length delimited. Does not implicitly {@link apipb.Response.verify|verify} messages.
         * @param message Response message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Response message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.Response;

        /**
         * Decodes a Response message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.Response;

        /**
         * Verifies a Response message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Response message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Response
         */
        public static fromObject(object: { [k: string]: any }): apipb.Response;

        /**
         * Creates a plain object from a Response message. Also converts values to other types if specified.
         * @param message Response
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.Response, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Response to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** LabelSelectorOperator enum. */
    enum LabelSelectorOperator {
        In = 0,
        NotIn = 1,
        Exists = 2,
        DoesNotExist = 3,
        Gt = 4,
        Lt = 5
    }

    /** Properties of a LabelSelectorRequirement. */
    interface ILabelSelectorRequirement {

        /** LabelSelectorRequirement key */
        key?: string;

        /** LabelSelectorRequirement operator */
        operator?: apipb.LabelSelectorOperator;

        /** LabelSelectorRequirement values */
        values?: string[];
    }

    /** Represents a LabelSelectorRequirement. */
    class LabelSelectorRequirement {

        /**
         * Constructs a new LabelSelectorRequirement.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.ILabelSelectorRequirement);

        /** LabelSelectorRequirement key. */
        public key: string;

        /** LabelSelectorRequirement operator. */
        public operator: apipb.LabelSelectorOperator;

        /** LabelSelectorRequirement values. */
        public values: string[];

        /**
         * Creates a new LabelSelectorRequirement instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LabelSelectorRequirement instance
         */
        public static create(properties?: apipb.ILabelSelectorRequirement): apipb.LabelSelectorRequirement;

        /**
         * Encodes the specified LabelSelectorRequirement message. Does not implicitly {@link apipb.LabelSelectorRequirement.verify|verify} messages.
         * @param message LabelSelectorRequirement message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.ILabelSelectorRequirement, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LabelSelectorRequirement message, length delimited. Does not implicitly {@link apipb.LabelSelectorRequirement.verify|verify} messages.
         * @param message LabelSelectorRequirement message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.ILabelSelectorRequirement, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LabelSelectorRequirement message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LabelSelectorRequirement
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.LabelSelectorRequirement;

        /**
         * Decodes a LabelSelectorRequirement message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LabelSelectorRequirement
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.LabelSelectorRequirement;

        /**
         * Verifies a LabelSelectorRequirement message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LabelSelectorRequirement message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LabelSelectorRequirement
         */
        public static fromObject(object: { [k: string]: any }): apipb.LabelSelectorRequirement;

        /**
         * Creates a plain object from a LabelSelectorRequirement message. Also converts values to other types if specified.
         * @param message LabelSelectorRequirement
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.LabelSelectorRequirement, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LabelSelectorRequirement to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a LabelSelectorTerm. */
    interface ILabelSelectorTerm {

        /** LabelSelectorTerm expressions */
        expressions?: apipb.ILabelSelectorRequirement[];
    }

    /** Represents a LabelSelectorTerm. */
    class LabelSelectorTerm {

        /**
         * Constructs a new LabelSelectorTerm.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.ILabelSelectorTerm);

        /** LabelSelectorTerm expressions. */
        public expressions: apipb.ILabelSelectorRequirement[];

        /**
         * Creates a new LabelSelectorTerm instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LabelSelectorTerm instance
         */
        public static create(properties?: apipb.ILabelSelectorTerm): apipb.LabelSelectorTerm;

        /**
         * Encodes the specified LabelSelectorTerm message. Does not implicitly {@link apipb.LabelSelectorTerm.verify|verify} messages.
         * @param message LabelSelectorTerm message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.ILabelSelectorTerm, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LabelSelectorTerm message, length delimited. Does not implicitly {@link apipb.LabelSelectorTerm.verify|verify} messages.
         * @param message LabelSelectorTerm message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.ILabelSelectorTerm, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LabelSelectorTerm message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LabelSelectorTerm
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.LabelSelectorTerm;

        /**
         * Decodes a LabelSelectorTerm message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LabelSelectorTerm
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.LabelSelectorTerm;

        /**
         * Verifies a LabelSelectorTerm message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LabelSelectorTerm message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LabelSelectorTerm
         */
        public static fromObject(object: { [k: string]: any }): apipb.LabelSelectorTerm;

        /**
         * Creates a plain object from a LabelSelectorTerm message. Also converts values to other types if specified.
         * @param message LabelSelectorTerm
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.LabelSelectorTerm, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LabelSelectorTerm to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a LabelSelector. */
    interface ILabelSelector {

        /** LabelSelector terms */
        terms?: apipb.ILabelSelectorTerm[];
    }

    /** Represents a LabelSelector. */
    class LabelSelector {

        /**
         * Constructs a new LabelSelector.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.ILabelSelector);

        /** LabelSelector terms. */
        public terms: apipb.ILabelSelectorTerm[];

        /**
         * Creates a new LabelSelector instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LabelSelector instance
         */
        public static create(properties?: apipb.ILabelSelector): apipb.LabelSelector;

        /**
         * Encodes the specified LabelSelector message. Does not implicitly {@link apipb.LabelSelector.verify|verify} messages.
         * @param message LabelSelector message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.ILabelSelector, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LabelSelector message, length delimited. Does not implicitly {@link apipb.LabelSelector.verify|verify} messages.
         * @param message LabelSelector message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.ILabelSelector, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LabelSelector message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LabelSelector
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.LabelSelector;

        /**
         * Decodes a LabelSelector message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LabelSelector
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.LabelSelector;

        /**
         * Verifies a LabelSelector message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LabelSelector message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LabelSelector
         */
        public static fromObject(object: { [k: string]: any }): apipb.LabelSelector;

        /**
         * Creates a plain object from a LabelSelector message. Also converts values to other types if specified.
         * @param message LabelSelector
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.LabelSelector, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LabelSelector to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an Address. */
    interface IAddress {

        /** Address streetAddress */
        streetAddress?: string;

        /** Address city */
        city?: string;

        /** Address state */
        state?: string;

        /** Address zipCode */
        zipCode?: string;

        /** Address countryCode */
        countryCode?: string;
    }

    /** Represents an Address. */
    class Address {

        /**
         * Constructs a new Address.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IAddress);

        /** Address streetAddress. */
        public streetAddress: string;

        /** Address city. */
        public city: string;

        /** Address state. */
        public state: string;

        /** Address zipCode. */
        public zipCode: string;

        /** Address countryCode. */
        public countryCode: string;

        /**
         * Creates a new Address instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Address instance
         */
        public static create(properties?: apipb.IAddress): apipb.Address;

        /**
         * Encodes the specified Address message. Does not implicitly {@link apipb.Address.verify|verify} messages.
         * @param message Address message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IAddress, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Address message, length delimited. Does not implicitly {@link apipb.Address.verify|verify} messages.
         * @param message Address message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IAddress, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an Address message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Address
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.Address;

        /**
         * Decodes an Address message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Address
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.Address;

        /**
         * Verifies an Address message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an Address message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Address
         */
        public static fromObject(object: { [k: string]: any }): apipb.Address;

        /**
         * Creates a plain object from an Address message. Also converts values to other types if specified.
         * @param message Address
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.Address, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Address to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Profile. */
    interface IProfile {

        /** Profile id */
        id?: string;

        /** Profile email */
        email?: string;

        /** Profile firstName */
        firstName?: string;

        /** Profile lastName */
        lastName?: string;

        /** Profile language */
        language?: string;

        /** Profile mobilePhone */
        mobilePhone?: string;

        /** Profile address */
        address?: apipb.IAddress;

        /** Profile createdAt */
        createdAt?: (number|Long);

        /** Profile lastUpdated */
        lastUpdated?: (number|Long);

        /** Profile locale */
        locale?: string;

        /** Profile timezone */
        timezone?: number;

        /** Profile country */
        country?: string;

        /** Profile labels */
        labels?: { [k: string]: string };
    }

    /** Represents a Profile. */
    class Profile {

        /**
         * Constructs a new Profile.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IProfile);

        /** Profile id. */
        public id: string;

        /** Profile email. */
        public email: string;

        /** Profile firstName. */
        public firstName: string;

        /** Profile lastName. */
        public lastName: string;

        /** Profile language. */
        public language: string;

        /** Profile mobilePhone. */
        public mobilePhone: string;

        /** Profile address. */
        public address?: (apipb.IAddress|null);

        /** Profile createdAt. */
        public createdAt: (number|Long);

        /** Profile lastUpdated. */
        public lastUpdated: (number|Long);

        /** Profile locale. */
        public locale: string;

        /** Profile timezone. */
        public timezone: number;

        /** Profile country. */
        public country: string;

        /** Profile labels. */
        public labels: { [k: string]: string };

        /**
         * Creates a new Profile instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Profile instance
         */
        public static create(properties?: apipb.IProfile): apipb.Profile;

        /**
         * Encodes the specified Profile message. Does not implicitly {@link apipb.Profile.verify|verify} messages.
         * @param message Profile message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IProfile, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Profile message, length delimited. Does not implicitly {@link apipb.Profile.verify|verify} messages.
         * @param message Profile message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IProfile, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Profile message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Profile
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.Profile;

        /**
         * Decodes a Profile message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Profile
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.Profile;

        /**
         * Verifies a Profile message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Profile message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Profile
         */
        public static fromObject(object: { [k: string]: any }): apipb.Profile;

        /**
         * Creates a plain object from a Profile message. Also converts values to other types if specified.
         * @param message Profile
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.Profile, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Profile to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a ChildGameEntry. */
    interface IChildGameEntry {

        /** ChildGameEntry id */
        id?: string;

        /** ChildGameEntry active */
        active?: boolean;

        /** ChildGameEntry dashboardIndex */
        dashboardIndex?: number;

        /** ChildGameEntry settings */
        settings?: Uint8Array;

        /** ChildGameEntry addedAt */
        addedAt?: (number|Long);

        /** ChildGameEntry activationChangedAt */
        activationChangedAt?: (number|Long);

        /** ChildGameEntry updatedAt */
        updatedAt?: (number|Long);

        /** ChildGameEntry localSettings */
        localSettings?: Uint8Array;

        /** ChildGameEntry localSettingsVersion */
        localSettingsVersion?: (number|Long);
    }

    /** Represents a ChildGameEntry. */
    class ChildGameEntry {

        /**
         * Constructs a new ChildGameEntry.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IChildGameEntry);

        /** ChildGameEntry id. */
        public id: string;

        /** ChildGameEntry active. */
        public active: boolean;

        /** ChildGameEntry dashboardIndex. */
        public dashboardIndex: number;

        /** ChildGameEntry settings. */
        public settings: Uint8Array;

        /** ChildGameEntry addedAt. */
        public addedAt: (number|Long);

        /** ChildGameEntry activationChangedAt. */
        public activationChangedAt: (number|Long);

        /** ChildGameEntry updatedAt. */
        public updatedAt: (number|Long);

        /** ChildGameEntry localSettings. */
        public localSettings: Uint8Array;

        /** ChildGameEntry localSettingsVersion. */
        public localSettingsVersion: (number|Long);

        /**
         * Creates a new ChildGameEntry instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ChildGameEntry instance
         */
        public static create(properties?: apipb.IChildGameEntry): apipb.ChildGameEntry;

        /**
         * Encodes the specified ChildGameEntry message. Does not implicitly {@link apipb.ChildGameEntry.verify|verify} messages.
         * @param message ChildGameEntry message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IChildGameEntry, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ChildGameEntry message, length delimited. Does not implicitly {@link apipb.ChildGameEntry.verify|verify} messages.
         * @param message ChildGameEntry message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IChildGameEntry, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ChildGameEntry message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ChildGameEntry
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.ChildGameEntry;

        /**
         * Decodes a ChildGameEntry message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ChildGameEntry
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.ChildGameEntry;

        /**
         * Verifies a ChildGameEntry message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ChildGameEntry message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ChildGameEntry
         */
        public static fromObject(object: { [k: string]: any }): apipb.ChildGameEntry;

        /**
         * Creates a plain object from a ChildGameEntry message. Also converts values to other types if specified.
         * @param message ChildGameEntry
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.ChildGameEntry, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ChildGameEntry to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Gender enum. */
    enum Gender {
        UNKNOWN = 0,
        MALE = 1,
        FEMALE = 2
    }

    /** Properties of a Child. */
    interface IChild {

        /** Child id */
        id?: string;

        /** Child parentId */
        parentId?: string;

        /** Child firstName */
        firstName?: string;

        /** Child lastName */
        lastName?: string;

        /** Child birthDay */
        birthDay?: (number|Long);

        /** Child gender */
        gender?: apipb.Gender;

        /** Child language */
        language?: string;

        /** Child games */
        games?: apipb.IChildGameEntry[];

        /** Child active */
        active?: boolean;

        /** Child loggedIn */
        loggedIn?: boolean;

        /** Child soundsEnabled */
        soundsEnabled?: boolean;

        /** Child locale */
        locale?: string;

        /** Child createdAt */
        createdAt?: (number|Long);

        /** Child lastUpdated */
        lastUpdated?: (number|Long);

        /** Child childInfo */
        childInfo?: { [k: string]: string };
    }

    /** Represents a Child. */
    class Child {

        /**
         * Constructs a new Child.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IChild);

        /** Child id. */
        public id: string;

        /** Child parentId. */
        public parentId: string;

        /** Child firstName. */
        public firstName: string;

        /** Child lastName. */
        public lastName: string;

        /** Child birthDay. */
        public birthDay: (number|Long);

        /** Child gender. */
        public gender: apipb.Gender;

        /** Child language. */
        public language: string;

        /** Child games. */
        public games: apipb.IChildGameEntry[];

        /** Child active. */
        public active: boolean;

        /** Child loggedIn. */
        public loggedIn: boolean;

        /** Child soundsEnabled. */
        public soundsEnabled: boolean;

        /** Child locale. */
        public locale: string;

        /** Child createdAt. */
        public createdAt: (number|Long);

        /** Child lastUpdated. */
        public lastUpdated: (number|Long);

        /** Child childInfo. */
        public childInfo: { [k: string]: string };

        /**
         * Creates a new Child instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Child instance
         */
        public static create(properties?: apipb.IChild): apipb.Child;

        /**
         * Encodes the specified Child message. Does not implicitly {@link apipb.Child.verify|verify} messages.
         * @param message Child message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IChild, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Child message, length delimited. Does not implicitly {@link apipb.Child.verify|verify} messages.
         * @param message Child message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IChild, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Child message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Child
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.Child;

        /**
         * Decodes a Child message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Child
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.Child;

        /**
         * Verifies a Child message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Child message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Child
         */
        public static fromObject(object: { [k: string]: any }): apipb.Child;

        /**
         * Creates a plain object from a Child message. Also converts values to other types if specified.
         * @param message Child
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.Child, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Child to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an Author. */
    interface IAuthor {

        /** Author name */
        name?: string;

        /** Author email */
        email?: string;
    }

    /** Represents an Author. */
    class Author {

        /**
         * Constructs a new Author.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IAuthor);

        /** Author name. */
        public name: string;

        /** Author email. */
        public email: string;

        /**
         * Creates a new Author instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Author instance
         */
        public static create(properties?: apipb.IAuthor): apipb.Author;

        /**
         * Encodes the specified Author message. Does not implicitly {@link apipb.Author.verify|verify} messages.
         * @param message Author message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IAuthor, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Author message, length delimited. Does not implicitly {@link apipb.Author.verify|verify} messages.
         * @param message Author message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IAuthor, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an Author message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Author
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.Author;

        /**
         * Decodes an Author message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Author
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.Author;

        /**
         * Verifies an Author message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an Author message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Author
         */
        public static fromObject(object: { [k: string]: any }): apipb.Author;

        /**
         * Creates a plain object from an Author message. Also converts values to other types if specified.
         * @param message Author
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.Author, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Author to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GameMetadata. */
    interface IGameMetadata {

        /** GameMetadata language */
        language?: string;

        /** GameMetadata visibleName */
        visibleName?: string;

        /** GameMetadata summary */
        summary?: string;

        /** GameMetadata description */
        description?: string;

        /** GameMetadata logo */
        logo?: string;

        /** GameMetadata icon */
        icon?: string;

        /** GameMetadata keywords */
        keywords?: string[];

        /** GameMetadata images */
        images?: string[];

        /** GameMetadata infoSlug */
        infoSlug?: string;

        /** GameMetadata assets */
        assets?: string[];
    }

    /** Represents a GameMetadata. */
    class GameMetadata {

        /**
         * Constructs a new GameMetadata.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGameMetadata);

        /** GameMetadata language. */
        public language: string;

        /** GameMetadata visibleName. */
        public visibleName: string;

        /** GameMetadata summary. */
        public summary: string;

        /** GameMetadata description. */
        public description: string;

        /** GameMetadata logo. */
        public logo: string;

        /** GameMetadata icon. */
        public icon: string;

        /** GameMetadata keywords. */
        public keywords: string[];

        /** GameMetadata images. */
        public images: string[];

        /** GameMetadata infoSlug. */
        public infoSlug: string;

        /** GameMetadata assets. */
        public assets: string[];

        /**
         * Creates a new GameMetadata instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameMetadata instance
         */
        public static create(properties?: apipb.IGameMetadata): apipb.GameMetadata;

        /**
         * Encodes the specified GameMetadata message. Does not implicitly {@link apipb.GameMetadata.verify|verify} messages.
         * @param message GameMetadata message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGameMetadata, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GameMetadata message, length delimited. Does not implicitly {@link apipb.GameMetadata.verify|verify} messages.
         * @param message GameMetadata message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGameMetadata, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GameMetadata message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameMetadata
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GameMetadata;

        /**
         * Decodes a GameMetadata message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameMetadata
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GameMetadata;

        /**
         * Verifies a GameMetadata message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GameMetadata message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GameMetadata
         */
        public static fromObject(object: { [k: string]: any }): apipb.GameMetadata;

        /**
         * Creates a plain object from a GameMetadata message. Also converts values to other types if specified.
         * @param message GameMetadata
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GameMetadata, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GameMetadata to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GameManifest. */
    interface IGameManifest {

        /** GameManifest uniqueName */
        uniqueName?: string;

        /** GameManifest licence */
        licence?: string;

        /** GameManifest languages */
        languages?: string[];

        /** GameManifest homepage */
        homepage?: string;

        /** GameManifest main */
        main?: string;

        /** GameManifest version */
        version?: string;

        /** GameManifest authors */
        authors?: apipb.IAuthor[];

        /** GameManifest repository */
        repository?: string;

        /** GameManifest supportedOrientations */
        supportedOrientations?: string[];

        /** GameManifest metadata */
        metadata?: apipb.IGameMetadata[];

        /** GameManifest exclude */
        exclude?: string[];

        /** GameManifest settings */
        settings?: string;

        /** GameManifest kvPath */
        kvPath?: string;

        /** GameManifest developerName */
        developerName?: string;

        /** GameManifest defaultLanguage */
        defaultLanguage?: string;

        /** GameManifest loadingColor */
        loadingColor?: string;

        /** GameManifest capabilities */
        capabilities?: string[];

        /** GameManifest abTest */
        abTest?: string;

        /** GameManifest labels */
        labels?: { [k: string]: string };

        /** GameManifest manifestVersion */
        manifestVersion?: number;
    }

    /** Represents a GameManifest. */
    class GameManifest {

        /**
         * Constructs a new GameManifest.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGameManifest);

        /** GameManifest uniqueName. */
        public uniqueName: string;

        /** GameManifest licence. */
        public licence: string;

        /** GameManifest languages. */
        public languages: string[];

        /** GameManifest homepage. */
        public homepage: string;

        /** GameManifest main. */
        public main: string;

        /** GameManifest version. */
        public version: string;

        /** GameManifest authors. */
        public authors: apipb.IAuthor[];

        /** GameManifest repository. */
        public repository: string;

        /** GameManifest supportedOrientations. */
        public supportedOrientations: string[];

        /** GameManifest metadata. */
        public metadata: apipb.IGameMetadata[];

        /** GameManifest exclude. */
        public exclude: string[];

        /** GameManifest settings. */
        public settings: string;

        /** GameManifest kvPath. */
        public kvPath: string;

        /** GameManifest developerName. */
        public developerName: string;

        /** GameManifest defaultLanguage. */
        public defaultLanguage: string;

        /** GameManifest loadingColor. */
        public loadingColor: string;

        /** GameManifest capabilities. */
        public capabilities: string[];

        /** GameManifest abTest. */
        public abTest: string;

        /** GameManifest labels. */
        public labels: { [k: string]: string };

        /** GameManifest manifestVersion. */
        public manifestVersion: number;

        /**
         * Creates a new GameManifest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameManifest instance
         */
        public static create(properties?: apipb.IGameManifest): apipb.GameManifest;

        /**
         * Encodes the specified GameManifest message. Does not implicitly {@link apipb.GameManifest.verify|verify} messages.
         * @param message GameManifest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGameManifest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GameManifest message, length delimited. Does not implicitly {@link apipb.GameManifest.verify|verify} messages.
         * @param message GameManifest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGameManifest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GameManifest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameManifest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GameManifest;

        /**
         * Decodes a GameManifest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameManifest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GameManifest;

        /**
         * Verifies a GameManifest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GameManifest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GameManifest
         */
        public static fromObject(object: { [k: string]: any }): apipb.GameManifest;

        /**
         * Creates a plain object from a GameManifest message. Also converts values to other types if specified.
         * @param message GameManifest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GameManifest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GameManifest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Game. */
    interface IGame {

        /** Game id */
        id?: string;

        /** Game uniqueName */
        uniqueName?: string;

        /** Game ownerId */
        ownerId?: string;

        /** Game productionVersion */
        productionVersion?: string;

        /** Game isOnProduction */
        isOnProduction?: boolean;

        /** Game createdAt */
        createdAt?: (number|Long);

        /** Game lastUpdated */
        lastUpdated?: (number|Long);

        /** Game labels */
        labels?: { [k: string]: string };
    }

    /** Represents a Game. */
    class Game {

        /**
         * Constructs a new Game.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGame);

        /** Game id. */
        public id: string;

        /** Game uniqueName. */
        public uniqueName: string;

        /** Game ownerId. */
        public ownerId: string;

        /** Game productionVersion. */
        public productionVersion: string;

        /** Game isOnProduction. */
        public isOnProduction: boolean;

        /** Game createdAt. */
        public createdAt: (number|Long);

        /** Game lastUpdated. */
        public lastUpdated: (number|Long);

        /** Game labels. */
        public labels: { [k: string]: string };

        /**
         * Creates a new Game instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Game instance
         */
        public static create(properties?: apipb.IGame): apipb.Game;

        /**
         * Encodes the specified Game message. Does not implicitly {@link apipb.Game.verify|verify} messages.
         * @param message Game message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Game message, length delimited. Does not implicitly {@link apipb.Game.verify|verify} messages.
         * @param message Game message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Game message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Game
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.Game;

        /**
         * Decodes a Game message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Game
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.Game;

        /**
         * Verifies a Game message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Game message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Game
         */
        public static fromObject(object: { [k: string]: any }): apipb.Game;

        /**
         * Creates a plain object from a Game message. Also converts values to other types if specified.
         * @param message Game
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.Game, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Game to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GameRelease. */
    interface IGameRelease {

        /** GameRelease releaseId */
        releaseId?: string;

        /** GameRelease gameId */
        gameId?: string;

        /** GameRelease version */
        version?: string;

        /** GameRelease gameManifest */
        gameManifest?: apipb.IGameManifest;

        /** GameRelease releasedAt */
        releasedAt?: (number|Long);

        /** GameRelease releasedBy */
        releasedBy?: string;

        /** GameRelease releaseState */
        releaseState?: apipb.ReleaseState;

        /** GameRelease validatedBy */
        validatedBy?: string;

        /** GameRelease validatedAt */
        validatedAt?: (number|Long);

        /** GameRelease intVersion */
        intVersion?: (number|Long);

        /** GameRelease storage */
        storage?: string;

        /** GameRelease archiveFormat */
        archiveFormat?: string;

        /** GameRelease packageUrl */
        packageUrl?: string;
    }

    /** Represents a GameRelease. */
    class GameRelease {

        /**
         * Constructs a new GameRelease.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IGameRelease);

        /** GameRelease releaseId. */
        public releaseId: string;

        /** GameRelease gameId. */
        public gameId: string;

        /** GameRelease version. */
        public version: string;

        /** GameRelease gameManifest. */
        public gameManifest?: (apipb.IGameManifest|null);

        /** GameRelease releasedAt. */
        public releasedAt: (number|Long);

        /** GameRelease releasedBy. */
        public releasedBy: string;

        /** GameRelease releaseState. */
        public releaseState: apipb.ReleaseState;

        /** GameRelease validatedBy. */
        public validatedBy: string;

        /** GameRelease validatedAt. */
        public validatedAt: (number|Long);

        /** GameRelease intVersion. */
        public intVersion: (number|Long);

        /** GameRelease storage. */
        public storage: string;

        /** GameRelease archiveFormat. */
        public archiveFormat: string;

        /** GameRelease packageUrl. */
        public packageUrl: string;

        /**
         * Creates a new GameRelease instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameRelease instance
         */
        public static create(properties?: apipb.IGameRelease): apipb.GameRelease;

        /**
         * Encodes the specified GameRelease message. Does not implicitly {@link apipb.GameRelease.verify|verify} messages.
         * @param message GameRelease message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IGameRelease, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GameRelease message, length delimited. Does not implicitly {@link apipb.GameRelease.verify|verify} messages.
         * @param message GameRelease message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IGameRelease, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GameRelease message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameRelease
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.GameRelease;

        /**
         * Decodes a GameRelease message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameRelease
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.GameRelease;

        /**
         * Verifies a GameRelease message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GameRelease message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GameRelease
         */
        public static fromObject(object: { [k: string]: any }): apipb.GameRelease;

        /**
         * Creates a plain object from a GameRelease message. Also converts values to other types if specified.
         * @param message GameRelease
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.GameRelease, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GameRelease to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** ReleaseState enum. */
    enum ReleaseState {
        CREATED = 0,
        DEVELOPMENT = 1,
        WAITING = 2,
        REJECTED = 3,
        VALIDATED = 4,
        PRODUCTION = 5
    }

    /** Properties of an UploadToken. */
    interface IUploadToken {

        /** UploadToken token */
        token?: string;

        /** UploadToken expiresAt */
        expiresAt?: (number|Long);

        /** UploadToken userId */
        userId?: string;

        /** UploadToken gameId */
        gameId?: string;

        /** UploadToken newVersion */
        newVersion?: string;

        /** UploadToken uploadTo */
        uploadTo?: string;
    }

    /** Represents an UploadToken. */
    class UploadToken {

        /**
         * Constructs a new UploadToken.
         * @param [properties] Properties to set
         */
        constructor(properties?: apipb.IUploadToken);

        /** UploadToken token. */
        public token: string;

        /** UploadToken expiresAt. */
        public expiresAt: (number|Long);

        /** UploadToken userId. */
        public userId: string;

        /** UploadToken gameId. */
        public gameId: string;

        /** UploadToken newVersion. */
        public newVersion: string;

        /** UploadToken uploadTo. */
        public uploadTo: string;

        /**
         * Creates a new UploadToken instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UploadToken instance
         */
        public static create(properties?: apipb.IUploadToken): apipb.UploadToken;

        /**
         * Encodes the specified UploadToken message. Does not implicitly {@link apipb.UploadToken.verify|verify} messages.
         * @param message UploadToken message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: apipb.IUploadToken, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified UploadToken message, length delimited. Does not implicitly {@link apipb.UploadToken.verify|verify} messages.
         * @param message UploadToken message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: apipb.IUploadToken, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an UploadToken message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UploadToken
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): apipb.UploadToken;

        /**
         * Decodes an UploadToken message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UploadToken
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): apipb.UploadToken;

        /**
         * Verifies an UploadToken message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an UploadToken message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns UploadToken
         */
        public static fromObject(object: { [k: string]: any }): apipb.UploadToken;

        /**
         * Creates a plain object from an UploadToken message. Also converts values to other types if specified.
         * @param message UploadToken
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: apipb.UploadToken, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this UploadToken to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}

/** Namespace google. */
export namespace google {

    /** Namespace api. */
    namespace api {

        /** Properties of a Http. */
        interface IHttp {

            /** Http rules */
            rules?: google.api.IHttpRule[];
        }

        /** Represents a Http. */
        class Http {

            /**
             * Constructs a new Http.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.api.IHttp);

            /** Http rules. */
            public rules: google.api.IHttpRule[];

            /**
             * Creates a new Http instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Http instance
             */
            public static create(properties?: google.api.IHttp): google.api.Http;

            /**
             * Encodes the specified Http message. Does not implicitly {@link google.api.Http.verify|verify} messages.
             * @param message Http message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.api.IHttp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Http message, length delimited. Does not implicitly {@link google.api.Http.verify|verify} messages.
             * @param message Http message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.api.IHttp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Http message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Http
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.api.Http;

            /**
             * Decodes a Http message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Http
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.api.Http;

            /**
             * Verifies a Http message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Http message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Http
             */
            public static fromObject(object: { [k: string]: any }): google.api.Http;

            /**
             * Creates a plain object from a Http message. Also converts values to other types if specified.
             * @param message Http
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.api.Http, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Http to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a HttpRule. */
        interface IHttpRule {

            /** HttpRule get */
            get?: string;

            /** HttpRule put */
            put?: string;

            /** HttpRule post */
            post?: string;

            /** HttpRule delete */
            "delete"?: string;

            /** HttpRule patch */
            patch?: string;

            /** HttpRule custom */
            custom?: google.api.ICustomHttpPattern;

            /** HttpRule selector */
            selector?: string;

            /** HttpRule body */
            body?: string;

            /** HttpRule additionalBindings */
            additionalBindings?: google.api.IHttpRule[];
        }

        /** Represents a HttpRule. */
        class HttpRule {

            /**
             * Constructs a new HttpRule.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.api.IHttpRule);

            /** HttpRule get. */
            public get: string;

            /** HttpRule put. */
            public put: string;

            /** HttpRule post. */
            public post: string;

            /** HttpRule delete. */
            public delete_: string;

            /** HttpRule patch. */
            public patch: string;

            /** HttpRule custom. */
            public custom?: (google.api.ICustomHttpPattern|null);

            /** HttpRule selector. */
            public selector: string;

            /** HttpRule body. */
            public body: string;

            /** HttpRule additionalBindings. */
            public additionalBindings: google.api.IHttpRule[];

            /** HttpRule pattern. */
            public pattern?: string;

            /**
             * Creates a new HttpRule instance using the specified properties.
             * @param [properties] Properties to set
             * @returns HttpRule instance
             */
            public static create(properties?: google.api.IHttpRule): google.api.HttpRule;

            /**
             * Encodes the specified HttpRule message. Does not implicitly {@link google.api.HttpRule.verify|verify} messages.
             * @param message HttpRule message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.api.IHttpRule, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified HttpRule message, length delimited. Does not implicitly {@link google.api.HttpRule.verify|verify} messages.
             * @param message HttpRule message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.api.IHttpRule, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a HttpRule message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns HttpRule
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.api.HttpRule;

            /**
             * Decodes a HttpRule message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns HttpRule
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.api.HttpRule;

            /**
             * Verifies a HttpRule message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a HttpRule message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns HttpRule
             */
            public static fromObject(object: { [k: string]: any }): google.api.HttpRule;

            /**
             * Creates a plain object from a HttpRule message. Also converts values to other types if specified.
             * @param message HttpRule
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.api.HttpRule, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this HttpRule to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a CustomHttpPattern. */
        interface ICustomHttpPattern {

            /** CustomHttpPattern kind */
            kind?: string;

            /** CustomHttpPattern path */
            path?: string;
        }

        /** Represents a CustomHttpPattern. */
        class CustomHttpPattern {

            /**
             * Constructs a new CustomHttpPattern.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.api.ICustomHttpPattern);

            /** CustomHttpPattern kind. */
            public kind: string;

            /** CustomHttpPattern path. */
            public path: string;

            /**
             * Creates a new CustomHttpPattern instance using the specified properties.
             * @param [properties] Properties to set
             * @returns CustomHttpPattern instance
             */
            public static create(properties?: google.api.ICustomHttpPattern): google.api.CustomHttpPattern;

            /**
             * Encodes the specified CustomHttpPattern message. Does not implicitly {@link google.api.CustomHttpPattern.verify|verify} messages.
             * @param message CustomHttpPattern message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.api.ICustomHttpPattern, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified CustomHttpPattern message, length delimited. Does not implicitly {@link google.api.CustomHttpPattern.verify|verify} messages.
             * @param message CustomHttpPattern message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.api.ICustomHttpPattern, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a CustomHttpPattern message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns CustomHttpPattern
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.api.CustomHttpPattern;

            /**
             * Decodes a CustomHttpPattern message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns CustomHttpPattern
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.api.CustomHttpPattern;

            /**
             * Verifies a CustomHttpPattern message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a CustomHttpPattern message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns CustomHttpPattern
             */
            public static fromObject(object: { [k: string]: any }): google.api.CustomHttpPattern;

            /**
             * Creates a plain object from a CustomHttpPattern message. Also converts values to other types if specified.
             * @param message CustomHttpPattern
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.api.CustomHttpPattern, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this CustomHttpPattern to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }

    /** Namespace protobuf. */
    namespace protobuf {

        /** Properties of a FileDescriptorSet. */
        interface IFileDescriptorSet {

            /** FileDescriptorSet file */
            file?: google.protobuf.IFileDescriptorProto[];
        }

        /** Represents a FileDescriptorSet. */
        class FileDescriptorSet {

            /**
             * Constructs a new FileDescriptorSet.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IFileDescriptorSet);

            /** FileDescriptorSet file. */
            public file: google.protobuf.IFileDescriptorProto[];

            /**
             * Creates a new FileDescriptorSet instance using the specified properties.
             * @param [properties] Properties to set
             * @returns FileDescriptorSet instance
             */
            public static create(properties?: google.protobuf.IFileDescriptorSet): google.protobuf.FileDescriptorSet;

            /**
             * Encodes the specified FileDescriptorSet message. Does not implicitly {@link google.protobuf.FileDescriptorSet.verify|verify} messages.
             * @param message FileDescriptorSet message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IFileDescriptorSet, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified FileDescriptorSet message, length delimited. Does not implicitly {@link google.protobuf.FileDescriptorSet.verify|verify} messages.
             * @param message FileDescriptorSet message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IFileDescriptorSet, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a FileDescriptorSet message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns FileDescriptorSet
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.FileDescriptorSet;

            /**
             * Decodes a FileDescriptorSet message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns FileDescriptorSet
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.FileDescriptorSet;

            /**
             * Verifies a FileDescriptorSet message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a FileDescriptorSet message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns FileDescriptorSet
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.FileDescriptorSet;

            /**
             * Creates a plain object from a FileDescriptorSet message. Also converts values to other types if specified.
             * @param message FileDescriptorSet
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.FileDescriptorSet, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this FileDescriptorSet to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a FileDescriptorProto. */
        interface IFileDescriptorProto {

            /** FileDescriptorProto name */
            name?: string;

            /** FileDescriptorProto package */
            "package"?: string;

            /** FileDescriptorProto dependency */
            dependency?: string[];

            /** FileDescriptorProto publicDependency */
            publicDependency?: number[];

            /** FileDescriptorProto weakDependency */
            weakDependency?: number[];

            /** FileDescriptorProto messageType */
            messageType?: google.protobuf.IDescriptorProto[];

            /** FileDescriptorProto enumType */
            enumType?: google.protobuf.IEnumDescriptorProto[];

            /** FileDescriptorProto service */
            service?: google.protobuf.IServiceDescriptorProto[];

            /** FileDescriptorProto extension */
            extension?: google.protobuf.IFieldDescriptorProto[];

            /** FileDescriptorProto options */
            options?: google.protobuf.IFileOptions;

            /** FileDescriptorProto sourceCodeInfo */
            sourceCodeInfo?: google.protobuf.ISourceCodeInfo;

            /** FileDescriptorProto syntax */
            syntax?: string;
        }

        /** Represents a FileDescriptorProto. */
        class FileDescriptorProto {

            /**
             * Constructs a new FileDescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IFileDescriptorProto);

            /** FileDescriptorProto name. */
            public name: string;

            /** FileDescriptorProto package. */
            public package_: string;

            /** FileDescriptorProto dependency. */
            public dependency: string[];

            /** FileDescriptorProto publicDependency. */
            public publicDependency: number[];

            /** FileDescriptorProto weakDependency. */
            public weakDependency: number[];

            /** FileDescriptorProto messageType. */
            public messageType: google.protobuf.IDescriptorProto[];

            /** FileDescriptorProto enumType. */
            public enumType: google.protobuf.IEnumDescriptorProto[];

            /** FileDescriptorProto service. */
            public service: google.protobuf.IServiceDescriptorProto[];

            /** FileDescriptorProto extension. */
            public extension: google.protobuf.IFieldDescriptorProto[];

            /** FileDescriptorProto options. */
            public options?: (google.protobuf.IFileOptions|null);

            /** FileDescriptorProto sourceCodeInfo. */
            public sourceCodeInfo?: (google.protobuf.ISourceCodeInfo|null);

            /** FileDescriptorProto syntax. */
            public syntax: string;

            /**
             * Creates a new FileDescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns FileDescriptorProto instance
             */
            public static create(properties?: google.protobuf.IFileDescriptorProto): google.protobuf.FileDescriptorProto;

            /**
             * Encodes the specified FileDescriptorProto message. Does not implicitly {@link google.protobuf.FileDescriptorProto.verify|verify} messages.
             * @param message FileDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IFileDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified FileDescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.FileDescriptorProto.verify|verify} messages.
             * @param message FileDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IFileDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a FileDescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns FileDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.FileDescriptorProto;

            /**
             * Decodes a FileDescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns FileDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.FileDescriptorProto;

            /**
             * Verifies a FileDescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a FileDescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns FileDescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.FileDescriptorProto;

            /**
             * Creates a plain object from a FileDescriptorProto message. Also converts values to other types if specified.
             * @param message FileDescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.FileDescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this FileDescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a DescriptorProto. */
        interface IDescriptorProto {

            /** DescriptorProto name */
            name?: string;

            /** DescriptorProto field */
            field?: google.protobuf.IFieldDescriptorProto[];

            /** DescriptorProto extension */
            extension?: google.protobuf.IFieldDescriptorProto[];

            /** DescriptorProto nestedType */
            nestedType?: google.protobuf.IDescriptorProto[];

            /** DescriptorProto enumType */
            enumType?: google.protobuf.IEnumDescriptorProto[];

            /** DescriptorProto extensionRange */
            extensionRange?: google.protobuf.DescriptorProto.IExtensionRange[];

            /** DescriptorProto oneofDecl */
            oneofDecl?: google.protobuf.IOneofDescriptorProto[];

            /** DescriptorProto options */
            options?: google.protobuf.IMessageOptions;

            /** DescriptorProto reservedRange */
            reservedRange?: google.protobuf.DescriptorProto.IReservedRange[];

            /** DescriptorProto reservedName */
            reservedName?: string[];
        }

        /** Represents a DescriptorProto. */
        class DescriptorProto {

            /**
             * Constructs a new DescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IDescriptorProto);

            /** DescriptorProto name. */
            public name: string;

            /** DescriptorProto field. */
            public field: google.protobuf.IFieldDescriptorProto[];

            /** DescriptorProto extension. */
            public extension: google.protobuf.IFieldDescriptorProto[];

            /** DescriptorProto nestedType. */
            public nestedType: google.protobuf.IDescriptorProto[];

            /** DescriptorProto enumType. */
            public enumType: google.protobuf.IEnumDescriptorProto[];

            /** DescriptorProto extensionRange. */
            public extensionRange: google.protobuf.DescriptorProto.IExtensionRange[];

            /** DescriptorProto oneofDecl. */
            public oneofDecl: google.protobuf.IOneofDescriptorProto[];

            /** DescriptorProto options. */
            public options?: (google.protobuf.IMessageOptions|null);

            /** DescriptorProto reservedRange. */
            public reservedRange: google.protobuf.DescriptorProto.IReservedRange[];

            /** DescriptorProto reservedName. */
            public reservedName: string[];

            /**
             * Creates a new DescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns DescriptorProto instance
             */
            public static create(properties?: google.protobuf.IDescriptorProto): google.protobuf.DescriptorProto;

            /**
             * Encodes the specified DescriptorProto message. Does not implicitly {@link google.protobuf.DescriptorProto.verify|verify} messages.
             * @param message DescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified DescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.DescriptorProto.verify|verify} messages.
             * @param message DescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a DescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns DescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.DescriptorProto;

            /**
             * Decodes a DescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns DescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.DescriptorProto;

            /**
             * Verifies a DescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a DescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns DescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.DescriptorProto;

            /**
             * Creates a plain object from a DescriptorProto message. Also converts values to other types if specified.
             * @param message DescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.DescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this DescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace DescriptorProto {

            /** Properties of an ExtensionRange. */
            interface IExtensionRange {

                /** ExtensionRange start */
                start?: number;

                /** ExtensionRange end */
                end?: number;
            }

            /** Represents an ExtensionRange. */
            class ExtensionRange {

                /**
                 * Constructs a new ExtensionRange.
                 * @param [properties] Properties to set
                 */
                constructor(properties?: google.protobuf.DescriptorProto.IExtensionRange);

                /** ExtensionRange start. */
                public start: number;

                /** ExtensionRange end. */
                public end: number;

                /**
                 * Creates a new ExtensionRange instance using the specified properties.
                 * @param [properties] Properties to set
                 * @returns ExtensionRange instance
                 */
                public static create(properties?: google.protobuf.DescriptorProto.IExtensionRange): google.protobuf.DescriptorProto.ExtensionRange;

                /**
                 * Encodes the specified ExtensionRange message. Does not implicitly {@link google.protobuf.DescriptorProto.ExtensionRange.verify|verify} messages.
                 * @param message ExtensionRange message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encode(message: google.protobuf.DescriptorProto.IExtensionRange, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Encodes the specified ExtensionRange message, length delimited. Does not implicitly {@link google.protobuf.DescriptorProto.ExtensionRange.verify|verify} messages.
                 * @param message ExtensionRange message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encodeDelimited(message: google.protobuf.DescriptorProto.IExtensionRange, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Decodes an ExtensionRange message from the specified reader or buffer.
                 * @param reader Reader or buffer to decode from
                 * @param [length] Message length if known beforehand
                 * @returns ExtensionRange
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.DescriptorProto.ExtensionRange;

                /**
                 * Decodes an ExtensionRange message from the specified reader or buffer, length delimited.
                 * @param reader Reader or buffer to decode from
                 * @returns ExtensionRange
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.DescriptorProto.ExtensionRange;

                /**
                 * Verifies an ExtensionRange message.
                 * @param message Plain object to verify
                 * @returns `null` if valid, otherwise the reason why it is not
                 */
                public static verify(message: { [k: string]: any }): (string|null);

                /**
                 * Creates an ExtensionRange message from a plain object. Also converts values to their respective internal types.
                 * @param object Plain object
                 * @returns ExtensionRange
                 */
                public static fromObject(object: { [k: string]: any }): google.protobuf.DescriptorProto.ExtensionRange;

                /**
                 * Creates a plain object from an ExtensionRange message. Also converts values to other types if specified.
                 * @param message ExtensionRange
                 * @param [options] Conversion options
                 * @returns Plain object
                 */
                public static toObject(message: google.protobuf.DescriptorProto.ExtensionRange, options?: $protobuf.IConversionOptions): { [k: string]: any };

                /**
                 * Converts this ExtensionRange to JSON.
                 * @returns JSON object
                 */
                public toJSON(): { [k: string]: any };
            }

            /** Properties of a ReservedRange. */
            interface IReservedRange {

                /** ReservedRange start */
                start?: number;

                /** ReservedRange end */
                end?: number;
            }

            /** Represents a ReservedRange. */
            class ReservedRange {

                /**
                 * Constructs a new ReservedRange.
                 * @param [properties] Properties to set
                 */
                constructor(properties?: google.protobuf.DescriptorProto.IReservedRange);

                /** ReservedRange start. */
                public start: number;

                /** ReservedRange end. */
                public end: number;

                /**
                 * Creates a new ReservedRange instance using the specified properties.
                 * @param [properties] Properties to set
                 * @returns ReservedRange instance
                 */
                public static create(properties?: google.protobuf.DescriptorProto.IReservedRange): google.protobuf.DescriptorProto.ReservedRange;

                /**
                 * Encodes the specified ReservedRange message. Does not implicitly {@link google.protobuf.DescriptorProto.ReservedRange.verify|verify} messages.
                 * @param message ReservedRange message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encode(message: google.protobuf.DescriptorProto.IReservedRange, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Encodes the specified ReservedRange message, length delimited. Does not implicitly {@link google.protobuf.DescriptorProto.ReservedRange.verify|verify} messages.
                 * @param message ReservedRange message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encodeDelimited(message: google.protobuf.DescriptorProto.IReservedRange, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Decodes a ReservedRange message from the specified reader or buffer.
                 * @param reader Reader or buffer to decode from
                 * @param [length] Message length if known beforehand
                 * @returns ReservedRange
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.DescriptorProto.ReservedRange;

                /**
                 * Decodes a ReservedRange message from the specified reader or buffer, length delimited.
                 * @param reader Reader or buffer to decode from
                 * @returns ReservedRange
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.DescriptorProto.ReservedRange;

                /**
                 * Verifies a ReservedRange message.
                 * @param message Plain object to verify
                 * @returns `null` if valid, otherwise the reason why it is not
                 */
                public static verify(message: { [k: string]: any }): (string|null);

                /**
                 * Creates a ReservedRange message from a plain object. Also converts values to their respective internal types.
                 * @param object Plain object
                 * @returns ReservedRange
                 */
                public static fromObject(object: { [k: string]: any }): google.protobuf.DescriptorProto.ReservedRange;

                /**
                 * Creates a plain object from a ReservedRange message. Also converts values to other types if specified.
                 * @param message ReservedRange
                 * @param [options] Conversion options
                 * @returns Plain object
                 */
                public static toObject(message: google.protobuf.DescriptorProto.ReservedRange, options?: $protobuf.IConversionOptions): { [k: string]: any };

                /**
                 * Converts this ReservedRange to JSON.
                 * @returns JSON object
                 */
                public toJSON(): { [k: string]: any };
            }
        }

        /** Properties of a FieldDescriptorProto. */
        interface IFieldDescriptorProto {

            /** FieldDescriptorProto name */
            name?: string;

            /** FieldDescriptorProto number */
            number?: number;

            /** FieldDescriptorProto label */
            label?: google.protobuf.FieldDescriptorProto.Label;

            /** FieldDescriptorProto type */
            type?: google.protobuf.FieldDescriptorProto.Type;

            /** FieldDescriptorProto typeName */
            typeName?: string;

            /** FieldDescriptorProto extendee */
            extendee?: string;

            /** FieldDescriptorProto defaultValue */
            defaultValue?: string;

            /** FieldDescriptorProto oneofIndex */
            oneofIndex?: number;

            /** FieldDescriptorProto jsonName */
            jsonName?: string;

            /** FieldDescriptorProto options */
            options?: google.protobuf.IFieldOptions;
        }

        /** Represents a FieldDescriptorProto. */
        class FieldDescriptorProto {

            /**
             * Constructs a new FieldDescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IFieldDescriptorProto);

            /** FieldDescriptorProto name. */
            public name: string;

            /** FieldDescriptorProto number. */
            public number: number;

            /** FieldDescriptorProto label. */
            public label: google.protobuf.FieldDescriptorProto.Label;

            /** FieldDescriptorProto type. */
            public type: google.protobuf.FieldDescriptorProto.Type;

            /** FieldDescriptorProto typeName. */
            public typeName: string;

            /** FieldDescriptorProto extendee. */
            public extendee: string;

            /** FieldDescriptorProto defaultValue. */
            public defaultValue: string;

            /** FieldDescriptorProto oneofIndex. */
            public oneofIndex: number;

            /** FieldDescriptorProto jsonName. */
            public jsonName: string;

            /** FieldDescriptorProto options. */
            public options?: (google.protobuf.IFieldOptions|null);

            /**
             * Creates a new FieldDescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns FieldDescriptorProto instance
             */
            public static create(properties?: google.protobuf.IFieldDescriptorProto): google.protobuf.FieldDescriptorProto;

            /**
             * Encodes the specified FieldDescriptorProto message. Does not implicitly {@link google.protobuf.FieldDescriptorProto.verify|verify} messages.
             * @param message FieldDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IFieldDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified FieldDescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.FieldDescriptorProto.verify|verify} messages.
             * @param message FieldDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IFieldDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a FieldDescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns FieldDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.FieldDescriptorProto;

            /**
             * Decodes a FieldDescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns FieldDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.FieldDescriptorProto;

            /**
             * Verifies a FieldDescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a FieldDescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns FieldDescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.FieldDescriptorProto;

            /**
             * Creates a plain object from a FieldDescriptorProto message. Also converts values to other types if specified.
             * @param message FieldDescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.FieldDescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this FieldDescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace FieldDescriptorProto {

            /** Type enum. */
            enum Type {
                TYPE_DOUBLE = 1,
                TYPE_FLOAT = 2,
                TYPE_INT64 = 3,
                TYPE_UINT64 = 4,
                TYPE_INT32 = 5,
                TYPE_FIXED64 = 6,
                TYPE_FIXED32 = 7,
                TYPE_BOOL = 8,
                TYPE_STRING = 9,
                TYPE_GROUP = 10,
                TYPE_MESSAGE = 11,
                TYPE_BYTES = 12,
                TYPE_UINT32 = 13,
                TYPE_ENUM = 14,
                TYPE_SFIXED32 = 15,
                TYPE_SFIXED64 = 16,
                TYPE_SINT32 = 17,
                TYPE_SINT64 = 18
            }

            /** Label enum. */
            enum Label {
                LABEL_OPTIONAL = 1,
                LABEL_REQUIRED = 2,
                LABEL_REPEATED = 3
            }
        }

        /** Properties of an OneofDescriptorProto. */
        interface IOneofDescriptorProto {

            /** OneofDescriptorProto name */
            name?: string;

            /** OneofDescriptorProto options */
            options?: google.protobuf.IOneofOptions;
        }

        /** Represents an OneofDescriptorProto. */
        class OneofDescriptorProto {

            /**
             * Constructs a new OneofDescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IOneofDescriptorProto);

            /** OneofDescriptorProto name. */
            public name: string;

            /** OneofDescriptorProto options. */
            public options?: (google.protobuf.IOneofOptions|null);

            /**
             * Creates a new OneofDescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns OneofDescriptorProto instance
             */
            public static create(properties?: google.protobuf.IOneofDescriptorProto): google.protobuf.OneofDescriptorProto;

            /**
             * Encodes the specified OneofDescriptorProto message. Does not implicitly {@link google.protobuf.OneofDescriptorProto.verify|verify} messages.
             * @param message OneofDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IOneofDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified OneofDescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.OneofDescriptorProto.verify|verify} messages.
             * @param message OneofDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IOneofDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an OneofDescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns OneofDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.OneofDescriptorProto;

            /**
             * Decodes an OneofDescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns OneofDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.OneofDescriptorProto;

            /**
             * Verifies an OneofDescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an OneofDescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns OneofDescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.OneofDescriptorProto;

            /**
             * Creates a plain object from an OneofDescriptorProto message. Also converts values to other types if specified.
             * @param message OneofDescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.OneofDescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this OneofDescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an EnumDescriptorProto. */
        interface IEnumDescriptorProto {

            /** EnumDescriptorProto name */
            name?: string;

            /** EnumDescriptorProto value */
            value?: google.protobuf.IEnumValueDescriptorProto[];

            /** EnumDescriptorProto options */
            options?: google.protobuf.IEnumOptions;
        }

        /** Represents an EnumDescriptorProto. */
        class EnumDescriptorProto {

            /**
             * Constructs a new EnumDescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IEnumDescriptorProto);

            /** EnumDescriptorProto name. */
            public name: string;

            /** EnumDescriptorProto value. */
            public value: google.protobuf.IEnumValueDescriptorProto[];

            /** EnumDescriptorProto options. */
            public options?: (google.protobuf.IEnumOptions|null);

            /**
             * Creates a new EnumDescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns EnumDescriptorProto instance
             */
            public static create(properties?: google.protobuf.IEnumDescriptorProto): google.protobuf.EnumDescriptorProto;

            /**
             * Encodes the specified EnumDescriptorProto message. Does not implicitly {@link google.protobuf.EnumDescriptorProto.verify|verify} messages.
             * @param message EnumDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IEnumDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified EnumDescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.EnumDescriptorProto.verify|verify} messages.
             * @param message EnumDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IEnumDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an EnumDescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns EnumDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.EnumDescriptorProto;

            /**
             * Decodes an EnumDescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns EnumDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.EnumDescriptorProto;

            /**
             * Verifies an EnumDescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an EnumDescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns EnumDescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.EnumDescriptorProto;

            /**
             * Creates a plain object from an EnumDescriptorProto message. Also converts values to other types if specified.
             * @param message EnumDescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.EnumDescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this EnumDescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an EnumValueDescriptorProto. */
        interface IEnumValueDescriptorProto {

            /** EnumValueDescriptorProto name */
            name?: string;

            /** EnumValueDescriptorProto number */
            number?: number;

            /** EnumValueDescriptorProto options */
            options?: google.protobuf.IEnumValueOptions;
        }

        /** Represents an EnumValueDescriptorProto. */
        class EnumValueDescriptorProto {

            /**
             * Constructs a new EnumValueDescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IEnumValueDescriptorProto);

            /** EnumValueDescriptorProto name. */
            public name: string;

            /** EnumValueDescriptorProto number. */
            public number: number;

            /** EnumValueDescriptorProto options. */
            public options?: (google.protobuf.IEnumValueOptions|null);

            /**
             * Creates a new EnumValueDescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns EnumValueDescriptorProto instance
             */
            public static create(properties?: google.protobuf.IEnumValueDescriptorProto): google.protobuf.EnumValueDescriptorProto;

            /**
             * Encodes the specified EnumValueDescriptorProto message. Does not implicitly {@link google.protobuf.EnumValueDescriptorProto.verify|verify} messages.
             * @param message EnumValueDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IEnumValueDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified EnumValueDescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.EnumValueDescriptorProto.verify|verify} messages.
             * @param message EnumValueDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IEnumValueDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an EnumValueDescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns EnumValueDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.EnumValueDescriptorProto;

            /**
             * Decodes an EnumValueDescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns EnumValueDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.EnumValueDescriptorProto;

            /**
             * Verifies an EnumValueDescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an EnumValueDescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns EnumValueDescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.EnumValueDescriptorProto;

            /**
             * Creates a plain object from an EnumValueDescriptorProto message. Also converts values to other types if specified.
             * @param message EnumValueDescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.EnumValueDescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this EnumValueDescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a ServiceDescriptorProto. */
        interface IServiceDescriptorProto {

            /** ServiceDescriptorProto name */
            name?: string;

            /** ServiceDescriptorProto method */
            method?: google.protobuf.IMethodDescriptorProto[];

            /** ServiceDescriptorProto options */
            options?: google.protobuf.IServiceOptions;
        }

        /** Represents a ServiceDescriptorProto. */
        class ServiceDescriptorProto {

            /**
             * Constructs a new ServiceDescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IServiceDescriptorProto);

            /** ServiceDescriptorProto name. */
            public name: string;

            /** ServiceDescriptorProto method. */
            public method: google.protobuf.IMethodDescriptorProto[];

            /** ServiceDescriptorProto options. */
            public options?: (google.protobuf.IServiceOptions|null);

            /**
             * Creates a new ServiceDescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns ServiceDescriptorProto instance
             */
            public static create(properties?: google.protobuf.IServiceDescriptorProto): google.protobuf.ServiceDescriptorProto;

            /**
             * Encodes the specified ServiceDescriptorProto message. Does not implicitly {@link google.protobuf.ServiceDescriptorProto.verify|verify} messages.
             * @param message ServiceDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IServiceDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified ServiceDescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.ServiceDescriptorProto.verify|verify} messages.
             * @param message ServiceDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IServiceDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a ServiceDescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns ServiceDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.ServiceDescriptorProto;

            /**
             * Decodes a ServiceDescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns ServiceDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.ServiceDescriptorProto;

            /**
             * Verifies a ServiceDescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a ServiceDescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns ServiceDescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.ServiceDescriptorProto;

            /**
             * Creates a plain object from a ServiceDescriptorProto message. Also converts values to other types if specified.
             * @param message ServiceDescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.ServiceDescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this ServiceDescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a MethodDescriptorProto. */
        interface IMethodDescriptorProto {

            /** MethodDescriptorProto name */
            name?: string;

            /** MethodDescriptorProto inputType */
            inputType?: string;

            /** MethodDescriptorProto outputType */
            outputType?: string;

            /** MethodDescriptorProto options */
            options?: google.protobuf.IMethodOptions;

            /** MethodDescriptorProto clientStreaming */
            clientStreaming?: boolean;

            /** MethodDescriptorProto serverStreaming */
            serverStreaming?: boolean;
        }

        /** Represents a MethodDescriptorProto. */
        class MethodDescriptorProto {

            /**
             * Constructs a new MethodDescriptorProto.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IMethodDescriptorProto);

            /** MethodDescriptorProto name. */
            public name: string;

            /** MethodDescriptorProto inputType. */
            public inputType: string;

            /** MethodDescriptorProto outputType. */
            public outputType: string;

            /** MethodDescriptorProto options. */
            public options?: (google.protobuf.IMethodOptions|null);

            /** MethodDescriptorProto clientStreaming. */
            public clientStreaming: boolean;

            /** MethodDescriptorProto serverStreaming. */
            public serverStreaming: boolean;

            /**
             * Creates a new MethodDescriptorProto instance using the specified properties.
             * @param [properties] Properties to set
             * @returns MethodDescriptorProto instance
             */
            public static create(properties?: google.protobuf.IMethodDescriptorProto): google.protobuf.MethodDescriptorProto;

            /**
             * Encodes the specified MethodDescriptorProto message. Does not implicitly {@link google.protobuf.MethodDescriptorProto.verify|verify} messages.
             * @param message MethodDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IMethodDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified MethodDescriptorProto message, length delimited. Does not implicitly {@link google.protobuf.MethodDescriptorProto.verify|verify} messages.
             * @param message MethodDescriptorProto message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IMethodDescriptorProto, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a MethodDescriptorProto message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns MethodDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.MethodDescriptorProto;

            /**
             * Decodes a MethodDescriptorProto message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns MethodDescriptorProto
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.MethodDescriptorProto;

            /**
             * Verifies a MethodDescriptorProto message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a MethodDescriptorProto message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns MethodDescriptorProto
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.MethodDescriptorProto;

            /**
             * Creates a plain object from a MethodDescriptorProto message. Also converts values to other types if specified.
             * @param message MethodDescriptorProto
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.MethodDescriptorProto, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this MethodDescriptorProto to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a FileOptions. */
        interface IFileOptions {

            /** FileOptions javaPackage */
            javaPackage?: string;

            /** FileOptions javaOuterClassname */
            javaOuterClassname?: string;

            /** FileOptions javaMultipleFiles */
            javaMultipleFiles?: boolean;

            /** FileOptions javaGenerateEqualsAndHash */
            javaGenerateEqualsAndHash?: boolean;

            /** FileOptions javaStringCheckUtf8 */
            javaStringCheckUtf8?: boolean;

            /** FileOptions optimizeFor */
            optimizeFor?: google.protobuf.FileOptions.OptimizeMode;

            /** FileOptions goPackage */
            goPackage?: string;

            /** FileOptions ccGenericServices */
            ccGenericServices?: boolean;

            /** FileOptions javaGenericServices */
            javaGenericServices?: boolean;

            /** FileOptions pyGenericServices */
            pyGenericServices?: boolean;

            /** FileOptions deprecated */
            deprecated?: boolean;

            /** FileOptions ccEnableArenas */
            ccEnableArenas?: boolean;

            /** FileOptions objcClassPrefix */
            objcClassPrefix?: string;

            /** FileOptions csharpNamespace */
            csharpNamespace?: string;

            /** FileOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];
        }

        /** Represents a FileOptions. */
        class FileOptions {

            /**
             * Constructs a new FileOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IFileOptions);

            /** FileOptions javaPackage. */
            public javaPackage: string;

            /** FileOptions javaOuterClassname. */
            public javaOuterClassname: string;

            /** FileOptions javaMultipleFiles. */
            public javaMultipleFiles: boolean;

            /** FileOptions javaGenerateEqualsAndHash. */
            public javaGenerateEqualsAndHash: boolean;

            /** FileOptions javaStringCheckUtf8. */
            public javaStringCheckUtf8: boolean;

            /** FileOptions optimizeFor. */
            public optimizeFor: google.protobuf.FileOptions.OptimizeMode;

            /** FileOptions goPackage. */
            public goPackage: string;

            /** FileOptions ccGenericServices. */
            public ccGenericServices: boolean;

            /** FileOptions javaGenericServices. */
            public javaGenericServices: boolean;

            /** FileOptions pyGenericServices. */
            public pyGenericServices: boolean;

            /** FileOptions deprecated. */
            public deprecated: boolean;

            /** FileOptions ccEnableArenas. */
            public ccEnableArenas: boolean;

            /** FileOptions objcClassPrefix. */
            public objcClassPrefix: string;

            /** FileOptions csharpNamespace. */
            public csharpNamespace: string;

            /** FileOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new FileOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns FileOptions instance
             */
            public static create(properties?: google.protobuf.IFileOptions): google.protobuf.FileOptions;

            /**
             * Encodes the specified FileOptions message. Does not implicitly {@link google.protobuf.FileOptions.verify|verify} messages.
             * @param message FileOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IFileOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified FileOptions message, length delimited. Does not implicitly {@link google.protobuf.FileOptions.verify|verify} messages.
             * @param message FileOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IFileOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a FileOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns FileOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.FileOptions;

            /**
             * Decodes a FileOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns FileOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.FileOptions;

            /**
             * Verifies a FileOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a FileOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns FileOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.FileOptions;

            /**
             * Creates a plain object from a FileOptions message. Also converts values to other types if specified.
             * @param message FileOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.FileOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this FileOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace FileOptions {

            /** OptimizeMode enum. */
            enum OptimizeMode {
                SPEED = 1,
                CODE_SIZE = 2,
                LITE_RUNTIME = 3
            }
        }

        /** Properties of a MessageOptions. */
        interface IMessageOptions {

            /** MessageOptions messageSetWireFormat */
            messageSetWireFormat?: boolean;

            /** MessageOptions noStandardDescriptorAccessor */
            noStandardDescriptorAccessor?: boolean;

            /** MessageOptions deprecated */
            deprecated?: boolean;

            /** MessageOptions mapEntry */
            mapEntry?: boolean;

            /** MessageOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];
        }

        /** Represents a MessageOptions. */
        class MessageOptions {

            /**
             * Constructs a new MessageOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IMessageOptions);

            /** MessageOptions messageSetWireFormat. */
            public messageSetWireFormat: boolean;

            /** MessageOptions noStandardDescriptorAccessor. */
            public noStandardDescriptorAccessor: boolean;

            /** MessageOptions deprecated. */
            public deprecated: boolean;

            /** MessageOptions mapEntry. */
            public mapEntry: boolean;

            /** MessageOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new MessageOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns MessageOptions instance
             */
            public static create(properties?: google.protobuf.IMessageOptions): google.protobuf.MessageOptions;

            /**
             * Encodes the specified MessageOptions message. Does not implicitly {@link google.protobuf.MessageOptions.verify|verify} messages.
             * @param message MessageOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IMessageOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified MessageOptions message, length delimited. Does not implicitly {@link google.protobuf.MessageOptions.verify|verify} messages.
             * @param message MessageOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IMessageOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a MessageOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns MessageOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.MessageOptions;

            /**
             * Decodes a MessageOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns MessageOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.MessageOptions;

            /**
             * Verifies a MessageOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a MessageOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns MessageOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.MessageOptions;

            /**
             * Creates a plain object from a MessageOptions message. Also converts values to other types if specified.
             * @param message MessageOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.MessageOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this MessageOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a FieldOptions. */
        interface IFieldOptions {

            /** FieldOptions ctype */
            ctype?: google.protobuf.FieldOptions.CType;

            /** FieldOptions packed */
            packed?: boolean;

            /** FieldOptions jstype */
            jstype?: google.protobuf.FieldOptions.JSType;

            /** FieldOptions lazy */
            lazy?: boolean;

            /** FieldOptions deprecated */
            deprecated?: boolean;

            /** FieldOptions weak */
            weak?: boolean;

            /** FieldOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];
        }

        /** Represents a FieldOptions. */
        class FieldOptions {

            /**
             * Constructs a new FieldOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IFieldOptions);

            /** FieldOptions ctype. */
            public ctype: google.protobuf.FieldOptions.CType;

            /** FieldOptions packed. */
            public packed: boolean;

            /** FieldOptions jstype. */
            public jstype: google.protobuf.FieldOptions.JSType;

            /** FieldOptions lazy. */
            public lazy: boolean;

            /** FieldOptions deprecated. */
            public deprecated: boolean;

            /** FieldOptions weak. */
            public weak: boolean;

            /** FieldOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new FieldOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns FieldOptions instance
             */
            public static create(properties?: google.protobuf.IFieldOptions): google.protobuf.FieldOptions;

            /**
             * Encodes the specified FieldOptions message. Does not implicitly {@link google.protobuf.FieldOptions.verify|verify} messages.
             * @param message FieldOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IFieldOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified FieldOptions message, length delimited. Does not implicitly {@link google.protobuf.FieldOptions.verify|verify} messages.
             * @param message FieldOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IFieldOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a FieldOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns FieldOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.FieldOptions;

            /**
             * Decodes a FieldOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns FieldOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.FieldOptions;

            /**
             * Verifies a FieldOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a FieldOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns FieldOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.FieldOptions;

            /**
             * Creates a plain object from a FieldOptions message. Also converts values to other types if specified.
             * @param message FieldOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.FieldOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this FieldOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace FieldOptions {

            /** CType enum. */
            enum CType {
                STRING = 0,
                CORD = 1,
                STRING_PIECE = 2
            }

            /** JSType enum. */
            enum JSType {
                JS_NORMAL = 0,
                JS_STRING = 1,
                JS_NUMBER = 2
            }
        }

        /** Properties of an OneofOptions. */
        interface IOneofOptions {

            /** OneofOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];
        }

        /** Represents an OneofOptions. */
        class OneofOptions {

            /**
             * Constructs a new OneofOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IOneofOptions);

            /** OneofOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new OneofOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns OneofOptions instance
             */
            public static create(properties?: google.protobuf.IOneofOptions): google.protobuf.OneofOptions;

            /**
             * Encodes the specified OneofOptions message. Does not implicitly {@link google.protobuf.OneofOptions.verify|verify} messages.
             * @param message OneofOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IOneofOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified OneofOptions message, length delimited. Does not implicitly {@link google.protobuf.OneofOptions.verify|verify} messages.
             * @param message OneofOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IOneofOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an OneofOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns OneofOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.OneofOptions;

            /**
             * Decodes an OneofOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns OneofOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.OneofOptions;

            /**
             * Verifies an OneofOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an OneofOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns OneofOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.OneofOptions;

            /**
             * Creates a plain object from an OneofOptions message. Also converts values to other types if specified.
             * @param message OneofOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.OneofOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this OneofOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an EnumOptions. */
        interface IEnumOptions {

            /** EnumOptions allowAlias */
            allowAlias?: boolean;

            /** EnumOptions deprecated */
            deprecated?: boolean;

            /** EnumOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];
        }

        /** Represents an EnumOptions. */
        class EnumOptions {

            /**
             * Constructs a new EnumOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IEnumOptions);

            /** EnumOptions allowAlias. */
            public allowAlias: boolean;

            /** EnumOptions deprecated. */
            public deprecated: boolean;

            /** EnumOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new EnumOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns EnumOptions instance
             */
            public static create(properties?: google.protobuf.IEnumOptions): google.protobuf.EnumOptions;

            /**
             * Encodes the specified EnumOptions message. Does not implicitly {@link google.protobuf.EnumOptions.verify|verify} messages.
             * @param message EnumOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IEnumOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified EnumOptions message, length delimited. Does not implicitly {@link google.protobuf.EnumOptions.verify|verify} messages.
             * @param message EnumOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IEnumOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an EnumOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns EnumOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.EnumOptions;

            /**
             * Decodes an EnumOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns EnumOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.EnumOptions;

            /**
             * Verifies an EnumOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an EnumOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns EnumOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.EnumOptions;

            /**
             * Creates a plain object from an EnumOptions message. Also converts values to other types if specified.
             * @param message EnumOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.EnumOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this EnumOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an EnumValueOptions. */
        interface IEnumValueOptions {

            /** EnumValueOptions deprecated */
            deprecated?: boolean;

            /** EnumValueOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];
        }

        /** Represents an EnumValueOptions. */
        class EnumValueOptions {

            /**
             * Constructs a new EnumValueOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IEnumValueOptions);

            /** EnumValueOptions deprecated. */
            public deprecated: boolean;

            /** EnumValueOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new EnumValueOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns EnumValueOptions instance
             */
            public static create(properties?: google.protobuf.IEnumValueOptions): google.protobuf.EnumValueOptions;

            /**
             * Encodes the specified EnumValueOptions message. Does not implicitly {@link google.protobuf.EnumValueOptions.verify|verify} messages.
             * @param message EnumValueOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IEnumValueOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified EnumValueOptions message, length delimited. Does not implicitly {@link google.protobuf.EnumValueOptions.verify|verify} messages.
             * @param message EnumValueOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IEnumValueOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an EnumValueOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns EnumValueOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.EnumValueOptions;

            /**
             * Decodes an EnumValueOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns EnumValueOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.EnumValueOptions;

            /**
             * Verifies an EnumValueOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an EnumValueOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns EnumValueOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.EnumValueOptions;

            /**
             * Creates a plain object from an EnumValueOptions message. Also converts values to other types if specified.
             * @param message EnumValueOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.EnumValueOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this EnumValueOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a ServiceOptions. */
        interface IServiceOptions {

            /** ServiceOptions deprecated */
            deprecated?: boolean;

            /** ServiceOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];
        }

        /** Represents a ServiceOptions. */
        class ServiceOptions {

            /**
             * Constructs a new ServiceOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IServiceOptions);

            /** ServiceOptions deprecated. */
            public deprecated: boolean;

            /** ServiceOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new ServiceOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns ServiceOptions instance
             */
            public static create(properties?: google.protobuf.IServiceOptions): google.protobuf.ServiceOptions;

            /**
             * Encodes the specified ServiceOptions message. Does not implicitly {@link google.protobuf.ServiceOptions.verify|verify} messages.
             * @param message ServiceOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IServiceOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified ServiceOptions message, length delimited. Does not implicitly {@link google.protobuf.ServiceOptions.verify|verify} messages.
             * @param message ServiceOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IServiceOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a ServiceOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns ServiceOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.ServiceOptions;

            /**
             * Decodes a ServiceOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns ServiceOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.ServiceOptions;

            /**
             * Verifies a ServiceOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a ServiceOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns ServiceOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.ServiceOptions;

            /**
             * Creates a plain object from a ServiceOptions message. Also converts values to other types if specified.
             * @param message ServiceOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.ServiceOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this ServiceOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a MethodOptions. */
        interface IMethodOptions {

            /** MethodOptions deprecated */
            deprecated?: boolean;

            /** MethodOptions uninterpretedOption */
            uninterpretedOption?: google.protobuf.IUninterpretedOption[];

            /** MethodOptions .google.api.http */
            ".google.api.http"?: google.api.IHttpRule;
        }

        /** Represents a MethodOptions. */
        class MethodOptions {

            /**
             * Constructs a new MethodOptions.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IMethodOptions);

            /** MethodOptions deprecated. */
            public deprecated: boolean;

            /** MethodOptions uninterpretedOption. */
            public uninterpretedOption: google.protobuf.IUninterpretedOption[];

            /**
             * Creates a new MethodOptions instance using the specified properties.
             * @param [properties] Properties to set
             * @returns MethodOptions instance
             */
            public static create(properties?: google.protobuf.IMethodOptions): google.protobuf.MethodOptions;

            /**
             * Encodes the specified MethodOptions message. Does not implicitly {@link google.protobuf.MethodOptions.verify|verify} messages.
             * @param message MethodOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IMethodOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified MethodOptions message, length delimited. Does not implicitly {@link google.protobuf.MethodOptions.verify|verify} messages.
             * @param message MethodOptions message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IMethodOptions, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a MethodOptions message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns MethodOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.MethodOptions;

            /**
             * Decodes a MethodOptions message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns MethodOptions
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.MethodOptions;

            /**
             * Verifies a MethodOptions message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a MethodOptions message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns MethodOptions
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.MethodOptions;

            /**
             * Creates a plain object from a MethodOptions message. Also converts values to other types if specified.
             * @param message MethodOptions
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.MethodOptions, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this MethodOptions to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an UninterpretedOption. */
        interface IUninterpretedOption {

            /** UninterpretedOption name */
            name?: google.protobuf.UninterpretedOption.INamePart[];

            /** UninterpretedOption identifierValue */
            identifierValue?: string;

            /** UninterpretedOption positiveIntValue */
            positiveIntValue?: (number|Long);

            /** UninterpretedOption negativeIntValue */
            negativeIntValue?: (number|Long);

            /** UninterpretedOption doubleValue */
            doubleValue?: number;

            /** UninterpretedOption stringValue */
            stringValue?: Uint8Array;

            /** UninterpretedOption aggregateValue */
            aggregateValue?: string;
        }

        /** Represents an UninterpretedOption. */
        class UninterpretedOption {

            /**
             * Constructs a new UninterpretedOption.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IUninterpretedOption);

            /** UninterpretedOption name. */
            public name: google.protobuf.UninterpretedOption.INamePart[];

            /** UninterpretedOption identifierValue. */
            public identifierValue: string;

            /** UninterpretedOption positiveIntValue. */
            public positiveIntValue: (number|Long);

            /** UninterpretedOption negativeIntValue. */
            public negativeIntValue: (number|Long);

            /** UninterpretedOption doubleValue. */
            public doubleValue: number;

            /** UninterpretedOption stringValue. */
            public stringValue: Uint8Array;

            /** UninterpretedOption aggregateValue. */
            public aggregateValue: string;

            /**
             * Creates a new UninterpretedOption instance using the specified properties.
             * @param [properties] Properties to set
             * @returns UninterpretedOption instance
             */
            public static create(properties?: google.protobuf.IUninterpretedOption): google.protobuf.UninterpretedOption;

            /**
             * Encodes the specified UninterpretedOption message. Does not implicitly {@link google.protobuf.UninterpretedOption.verify|verify} messages.
             * @param message UninterpretedOption message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IUninterpretedOption, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified UninterpretedOption message, length delimited. Does not implicitly {@link google.protobuf.UninterpretedOption.verify|verify} messages.
             * @param message UninterpretedOption message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IUninterpretedOption, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an UninterpretedOption message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns UninterpretedOption
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.UninterpretedOption;

            /**
             * Decodes an UninterpretedOption message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns UninterpretedOption
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.UninterpretedOption;

            /**
             * Verifies an UninterpretedOption message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an UninterpretedOption message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns UninterpretedOption
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.UninterpretedOption;

            /**
             * Creates a plain object from an UninterpretedOption message. Also converts values to other types if specified.
             * @param message UninterpretedOption
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.UninterpretedOption, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this UninterpretedOption to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace UninterpretedOption {

            /** Properties of a NamePart. */
            interface INamePart {

                /** NamePart namePart */
                namePart: string;

                /** NamePart isExtension */
                isExtension: boolean;
            }

            /** Represents a NamePart. */
            class NamePart {

                /**
                 * Constructs a new NamePart.
                 * @param [properties] Properties to set
                 */
                constructor(properties?: google.protobuf.UninterpretedOption.INamePart);

                /** NamePart namePart. */
                public namePart: string;

                /** NamePart isExtension. */
                public isExtension: boolean;

                /**
                 * Creates a new NamePart instance using the specified properties.
                 * @param [properties] Properties to set
                 * @returns NamePart instance
                 */
                public static create(properties?: google.protobuf.UninterpretedOption.INamePart): google.protobuf.UninterpretedOption.NamePart;

                /**
                 * Encodes the specified NamePart message. Does not implicitly {@link google.protobuf.UninterpretedOption.NamePart.verify|verify} messages.
                 * @param message NamePart message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encode(message: google.protobuf.UninterpretedOption.INamePart, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Encodes the specified NamePart message, length delimited. Does not implicitly {@link google.protobuf.UninterpretedOption.NamePart.verify|verify} messages.
                 * @param message NamePart message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encodeDelimited(message: google.protobuf.UninterpretedOption.INamePart, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Decodes a NamePart message from the specified reader or buffer.
                 * @param reader Reader or buffer to decode from
                 * @param [length] Message length if known beforehand
                 * @returns NamePart
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.UninterpretedOption.NamePart;

                /**
                 * Decodes a NamePart message from the specified reader or buffer, length delimited.
                 * @param reader Reader or buffer to decode from
                 * @returns NamePart
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.UninterpretedOption.NamePart;

                /**
                 * Verifies a NamePart message.
                 * @param message Plain object to verify
                 * @returns `null` if valid, otherwise the reason why it is not
                 */
                public static verify(message: { [k: string]: any }): (string|null);

                /**
                 * Creates a NamePart message from a plain object. Also converts values to their respective internal types.
                 * @param object Plain object
                 * @returns NamePart
                 */
                public static fromObject(object: { [k: string]: any }): google.protobuf.UninterpretedOption.NamePart;

                /**
                 * Creates a plain object from a NamePart message. Also converts values to other types if specified.
                 * @param message NamePart
                 * @param [options] Conversion options
                 * @returns Plain object
                 */
                public static toObject(message: google.protobuf.UninterpretedOption.NamePart, options?: $protobuf.IConversionOptions): { [k: string]: any };

                /**
                 * Converts this NamePart to JSON.
                 * @returns JSON object
                 */
                public toJSON(): { [k: string]: any };
            }
        }

        /** Properties of a SourceCodeInfo. */
        interface ISourceCodeInfo {

            /** SourceCodeInfo location */
            location?: google.protobuf.SourceCodeInfo.ILocation[];
        }

        /** Represents a SourceCodeInfo. */
        class SourceCodeInfo {

            /**
             * Constructs a new SourceCodeInfo.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.ISourceCodeInfo);

            /** SourceCodeInfo location. */
            public location: google.protobuf.SourceCodeInfo.ILocation[];

            /**
             * Creates a new SourceCodeInfo instance using the specified properties.
             * @param [properties] Properties to set
             * @returns SourceCodeInfo instance
             */
            public static create(properties?: google.protobuf.ISourceCodeInfo): google.protobuf.SourceCodeInfo;

            /**
             * Encodes the specified SourceCodeInfo message. Does not implicitly {@link google.protobuf.SourceCodeInfo.verify|verify} messages.
             * @param message SourceCodeInfo message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.ISourceCodeInfo, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified SourceCodeInfo message, length delimited. Does not implicitly {@link google.protobuf.SourceCodeInfo.verify|verify} messages.
             * @param message SourceCodeInfo message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.ISourceCodeInfo, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a SourceCodeInfo message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns SourceCodeInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.SourceCodeInfo;

            /**
             * Decodes a SourceCodeInfo message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns SourceCodeInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.SourceCodeInfo;

            /**
             * Verifies a SourceCodeInfo message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a SourceCodeInfo message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns SourceCodeInfo
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.SourceCodeInfo;

            /**
             * Creates a plain object from a SourceCodeInfo message. Also converts values to other types if specified.
             * @param message SourceCodeInfo
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.SourceCodeInfo, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this SourceCodeInfo to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace SourceCodeInfo {

            /** Properties of a Location. */
            interface ILocation {

                /** Location path */
                path?: number[];

                /** Location span */
                span?: number[];

                /** Location leadingComments */
                leadingComments?: string;

                /** Location trailingComments */
                trailingComments?: string;

                /** Location leadingDetachedComments */
                leadingDetachedComments?: string[];
            }

            /** Represents a Location. */
            class Location {

                /**
                 * Constructs a new Location.
                 * @param [properties] Properties to set
                 */
                constructor(properties?: google.protobuf.SourceCodeInfo.ILocation);

                /** Location path. */
                public path: number[];

                /** Location span. */
                public span: number[];

                /** Location leadingComments. */
                public leadingComments: string;

                /** Location trailingComments. */
                public trailingComments: string;

                /** Location leadingDetachedComments. */
                public leadingDetachedComments: string[];

                /**
                 * Creates a new Location instance using the specified properties.
                 * @param [properties] Properties to set
                 * @returns Location instance
                 */
                public static create(properties?: google.protobuf.SourceCodeInfo.ILocation): google.protobuf.SourceCodeInfo.Location;

                /**
                 * Encodes the specified Location message. Does not implicitly {@link google.protobuf.SourceCodeInfo.Location.verify|verify} messages.
                 * @param message Location message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encode(message: google.protobuf.SourceCodeInfo.ILocation, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Encodes the specified Location message, length delimited. Does not implicitly {@link google.protobuf.SourceCodeInfo.Location.verify|verify} messages.
                 * @param message Location message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encodeDelimited(message: google.protobuf.SourceCodeInfo.ILocation, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Decodes a Location message from the specified reader or buffer.
                 * @param reader Reader or buffer to decode from
                 * @param [length] Message length if known beforehand
                 * @returns Location
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.SourceCodeInfo.Location;

                /**
                 * Decodes a Location message from the specified reader or buffer, length delimited.
                 * @param reader Reader or buffer to decode from
                 * @returns Location
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.SourceCodeInfo.Location;

                /**
                 * Verifies a Location message.
                 * @param message Plain object to verify
                 * @returns `null` if valid, otherwise the reason why it is not
                 */
                public static verify(message: { [k: string]: any }): (string|null);

                /**
                 * Creates a Location message from a plain object. Also converts values to their respective internal types.
                 * @param object Plain object
                 * @returns Location
                 */
                public static fromObject(object: { [k: string]: any }): google.protobuf.SourceCodeInfo.Location;

                /**
                 * Creates a plain object from a Location message. Also converts values to other types if specified.
                 * @param message Location
                 * @param [options] Conversion options
                 * @returns Plain object
                 */
                public static toObject(message: google.protobuf.SourceCodeInfo.Location, options?: $protobuf.IConversionOptions): { [k: string]: any };

                /**
                 * Converts this Location to JSON.
                 * @returns JSON object
                 */
                public toJSON(): { [k: string]: any };
            }
        }

        /** Properties of a GeneratedCodeInfo. */
        interface IGeneratedCodeInfo {

            /** GeneratedCodeInfo annotation */
            annotation?: google.protobuf.GeneratedCodeInfo.IAnnotation[];
        }

        /** Represents a GeneratedCodeInfo. */
        class GeneratedCodeInfo {

            /**
             * Constructs a new GeneratedCodeInfo.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IGeneratedCodeInfo);

            /** GeneratedCodeInfo annotation. */
            public annotation: google.protobuf.GeneratedCodeInfo.IAnnotation[];

            /**
             * Creates a new GeneratedCodeInfo instance using the specified properties.
             * @param [properties] Properties to set
             * @returns GeneratedCodeInfo instance
             */
            public static create(properties?: google.protobuf.IGeneratedCodeInfo): google.protobuf.GeneratedCodeInfo;

            /**
             * Encodes the specified GeneratedCodeInfo message. Does not implicitly {@link google.protobuf.GeneratedCodeInfo.verify|verify} messages.
             * @param message GeneratedCodeInfo message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IGeneratedCodeInfo, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified GeneratedCodeInfo message, length delimited. Does not implicitly {@link google.protobuf.GeneratedCodeInfo.verify|verify} messages.
             * @param message GeneratedCodeInfo message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IGeneratedCodeInfo, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a GeneratedCodeInfo message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns GeneratedCodeInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.GeneratedCodeInfo;

            /**
             * Decodes a GeneratedCodeInfo message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns GeneratedCodeInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.GeneratedCodeInfo;

            /**
             * Verifies a GeneratedCodeInfo message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a GeneratedCodeInfo message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns GeneratedCodeInfo
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.GeneratedCodeInfo;

            /**
             * Creates a plain object from a GeneratedCodeInfo message. Also converts values to other types if specified.
             * @param message GeneratedCodeInfo
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.GeneratedCodeInfo, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this GeneratedCodeInfo to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        namespace GeneratedCodeInfo {

            /** Properties of an Annotation. */
            interface IAnnotation {

                /** Annotation path */
                path?: number[];

                /** Annotation sourceFile */
                sourceFile?: string;

                /** Annotation begin */
                begin?: number;

                /** Annotation end */
                end?: number;
            }

            /** Represents an Annotation. */
            class Annotation {

                /**
                 * Constructs a new Annotation.
                 * @param [properties] Properties to set
                 */
                constructor(properties?: google.protobuf.GeneratedCodeInfo.IAnnotation);

                /** Annotation path. */
                public path: number[];

                /** Annotation sourceFile. */
                public sourceFile: string;

                /** Annotation begin. */
                public begin: number;

                /** Annotation end. */
                public end: number;

                /**
                 * Creates a new Annotation instance using the specified properties.
                 * @param [properties] Properties to set
                 * @returns Annotation instance
                 */
                public static create(properties?: google.protobuf.GeneratedCodeInfo.IAnnotation): google.protobuf.GeneratedCodeInfo.Annotation;

                /**
                 * Encodes the specified Annotation message. Does not implicitly {@link google.protobuf.GeneratedCodeInfo.Annotation.verify|verify} messages.
                 * @param message Annotation message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encode(message: google.protobuf.GeneratedCodeInfo.IAnnotation, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Encodes the specified Annotation message, length delimited. Does not implicitly {@link google.protobuf.GeneratedCodeInfo.Annotation.verify|verify} messages.
                 * @param message Annotation message or plain object to encode
                 * @param [writer] Writer to encode to
                 * @returns Writer
                 */
                public static encodeDelimited(message: google.protobuf.GeneratedCodeInfo.IAnnotation, writer?: $protobuf.Writer): $protobuf.Writer;

                /**
                 * Decodes an Annotation message from the specified reader or buffer.
                 * @param reader Reader or buffer to decode from
                 * @param [length] Message length if known beforehand
                 * @returns Annotation
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.GeneratedCodeInfo.Annotation;

                /**
                 * Decodes an Annotation message from the specified reader or buffer, length delimited.
                 * @param reader Reader or buffer to decode from
                 * @returns Annotation
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.GeneratedCodeInfo.Annotation;

                /**
                 * Verifies an Annotation message.
                 * @param message Plain object to verify
                 * @returns `null` if valid, otherwise the reason why it is not
                 */
                public static verify(message: { [k: string]: any }): (string|null);

                /**
                 * Creates an Annotation message from a plain object. Also converts values to their respective internal types.
                 * @param object Plain object
                 * @returns Annotation
                 */
                public static fromObject(object: { [k: string]: any }): google.protobuf.GeneratedCodeInfo.Annotation;

                /**
                 * Creates a plain object from an Annotation message. Also converts values to other types if specified.
                 * @param message Annotation
                 * @param [options] Conversion options
                 * @returns Plain object
                 */
                public static toObject(message: google.protobuf.GeneratedCodeInfo.Annotation, options?: $protobuf.IConversionOptions): { [k: string]: any };

                /**
                 * Converts this Annotation to JSON.
                 * @returns JSON object
                 */
                public toJSON(): { [k: string]: any };
            }
        }
    }
}
