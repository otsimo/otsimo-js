import { Otsimo, getAllUrlParams } from "./common";
import oand from "./android";

export interface TTSDriver {
  speak(text: string): void;
  voiceList(): string[];
  getVoice(): string;
  setVoice(voice: string): void;
}

class iOSDriver implements TTSDriver {
  private w: any;
  private _v: string = "";
  private _vl: string[] = [];

  constructor() {
    this.w = window as any;
    let p = getAllUrlParams();
    this._v = p["voice"] || "";
    this._vl = p["voicelist"] || [];
  }

  speak(text: string): void {
    this.w.webkit.messageHandlers.tts.postMessage({
      event: "speak",
      data: text,
    });
  }

  voiceList(): string[] {
    return this._vl;
  }

  getVoice(): string {
    return this._v;
  }

  setVoice(voice: string): void {
    this.w.webkit.messageHandlers.tts.postMessage({
      event: "setVoice",
      data: voice,
    });
    this._v = voice;
  }
}

class AndroidDriver implements TTSDriver {
  private _v: string = "";
  private _vl: string[] = [];
  private otsimo: Otsimo<any, any, any>;

  constructor(otsimo: Otsimo<any, any, any>) {
    let p = getAllUrlParams();
    this.otsimo = otsimo;
    this._v = p["voice"] || "";
    this._vl = p["voicelist"] || [];
  }

  speak(text: string): void {
    if (this.otsimo.androidVersion >= 20) {
      oand.ttsSpeak(text);
    } else {
      window.postMessage(
        JSON.stringify({ action: "tts", event: "speak", data: text }),
        "*",
      );
    }
  }

  voiceList(): string[] {
    return this._vl;
  }

  getVoice(): string {
    return this._v;
  }

  setVoice(voice: string): void {
    window.postMessage(
      JSON.stringify({ action: "tts", event: "setVoice", data: voice }),
      "*",
    );
  }
}

export class TTS {
  private _driver: TTSDriver;
  constructor(private otsimo: Otsimo<any, any, any>) {
    if (otsimo.isWKWebView) {
      this._driver = new iOSDriver();
    } else if (otsimo.android) {
      this._driver = new AndroidDriver(otsimo);
    }
  }

  speak(text: string) {
    if (this._driver) {
      return this._driver.speak(text);
    }
    console.error(new Error("TTS Driver is not set"));
  }

  setVoice(voice: string) {
    if (this._driver) {
      return this._driver.setVoice(voice);
    }
    console.error(new Error("TTS Driver is not set"));
  }

  getVoice(): string {
    if (this._driver) {
      return this._driver.getVoice();
    }
    console.error(new Error("TTS Driver is not set"));
    return "";
  }

  voiceList(): string[] {
    if (this._driver) {
      return this._driver.voiceList();
    }
    console.error(new Error("TTS Driver is not set"));
    return [];
  }

  setDriver(driver: TTSDriver) {
    this._driver = driver;
  }

  getDriver(): TTSDriver {
    return this._driver;
  }
}
