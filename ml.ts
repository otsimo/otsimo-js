import { Otsimo, InputSource, MLApi } from "./common";

export class ML implements MLApi {
  constructor(private otsimo: Otsimo<any, any, any>) {}

  loadGraph(filepath: string) {}

  predict(input: any, cb: (result: any) => void) {}

  predictFromStream(source: InputSource, cb: (result: any) => void) {}

  closeStream() {}
}
